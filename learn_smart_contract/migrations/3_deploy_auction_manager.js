const AuctionManager = artifacts.require("AuctionManager");

module.exports = function(deployer) {
  // Deploy the AuctionManager contract
  deployer.deploy(AuctionManager);
};