const Auction = artifacts.require("Auction");

module.exports = function(deployer, network, accounts) {
  // Your deployment logic here
  const startTimestamp = Math.floor(new Date().getTime() / 1000) + 3600; // 1 hour from now
  deployer.deploy(Auction, "ItemName", 60, 404, startTimestamp, accounts[0]);
};