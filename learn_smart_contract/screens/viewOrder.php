<?php
    include_once 'header.php';
    include ('dataconnection.php');
?>

<!-- Bootstrap CSS -->

<!-- Bootstrap JS and its dependencies -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<!--Body Content-->
<div id="page-content">
    	<!--Page Title-->
    	<div class="page section-header text-center">
			<div class="page-title">
        		<div class="wrapper"><h1 class="page-width">My Orders</h1></div>
      		</div>
		</div>
        <!--End Page Title-->
        
        <div class="container">
        	<div class="row">
            <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 mb-3">
                    <div class="customer-box returning-customer">
                    <?php
                        if(isset($_SESSION["userID"])) 
                        {
                            //fetch all active orders 
                            $sql = "SELECT * from auction WHERE winner_userid = '".$_SESSION["userID"]."' AND status = 'settlement' AND (CONVERT_TZ(NOW(), @@session.time_zone, '+08:00') > EndTime)";
                            $result = mysqli_query($connect, $sql);                            
                            echo "<h3><i class='icon anm anm-user-al'></i> Active Orders <a class='text-white text-decoration-underline' data-toggle='collapse'></a></h3>";
                            while ($row = mysqli_fetch_assoc($result))
                            {
                            ?>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div class="your-order-payment">
                                    <div class="your-order">
                                        <h2 class="order-title mb-4">Auction <?php echo $row['auctionID']; ?> [Status: <?php echo $row['status']; ?>]</h2>

                                        <div class="table-responsive-sm order-table"> 
                                            <table id="cartTable" class="bg-white table table-bordered table-hover text-center">
                                                <thead>
                                                    <tr>
                                                        <th class="text-left">Product Name</th>
                                                        <th>Front Image</th>
                                                        <th>Back Image</th>
                                                        <th>Condition</th>
                                                        <th>Description</th>
                                                        <th>Auction Start DateTime</th>
                                                        <th>Auction End DateTime</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $sqll = "SELECT auctionID, ItemName, product_front_image, product_back_image, product_condition, description, StartTime, EndTime, reserved_price, highest_bid_bidid, auction_contract_address FROM auction WHERE AuctionID = '".$row['AuctionID']."'";
                                                        $resultt = mysqli_query($connect, $sqll);
                                                        
                                                        while ($row2 = mysqli_fetch_assoc($resultt))
                                                        {

                                                            $prodResult = mysqli_query($connect, "SELECT auctionID, ItemName, product_front_image, product_back_image, product_condition, description, StartTime, EndTime, reserved_price, highest_bid_bidid, auction_contract_address FROM auction WHERE AuctionID = '".$row['AuctionID']."'");
                                                            $prodRow = mysqli_fetch_assoc($prodResult);
                                                    ?>
                                                    <tr>
                                                        <td class="text-left"><a href="<?php printf('%s?auctionID=%s', 'product-layout.php',  $prodRow['auctionID']); ?>"><?php echo $prodRow['ItemName'];  ?></a><input name="auction_contract_address" class="auction_contract_address" type="hidden" data-auction-id="<?php echo $prodRow['auctionID']; ?>" value="<?php echo $prodRow['auction_contract_address']; ?>"></td>
                                                        <td><img src="./assets/images/productimg/<?php echo $prodRow['product_front_image']; ?>" width="100" height="100"></td>
                                                        <td><img src="./assets/images/productimg/<?php echo $prodRow['product_back_image']; ?>" width="100" height="100"></td>
                                                        <td><?php echo $row2['product_condition']; ?></td>
                                                        <td><?php echo $row2['description']; ?></td>
                                                        <td><?php echo $prodRow['StartTime']; ?></td>
                                                        <td><?php echo $prodRow['EndTime']; ?></td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                                <tfoot class="font-weight-600">
                                                    <tr>
                                                        <!--
                                                        <td colspan="4" class="text-right">Shipping </td>
                                                        <td>RM 50.00</td>
                                                    </tr>
                                                        -->
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>          
                                </div>
                                <div class="order-button-payment">
                                    <button class="btn btn-enter-delivery" value="View Delivery Address" type="button" onclick="enterDeliveryAddressModal('<?php echo $prodRow['auctionID']; ?>')" >View Delivery Address</button>
                                    <button class="btn btn-release-fund" value="Release Fund" type="button" onclick="releaseFund('<?php echo $prodRow['auctionID']; ?>')" >Order Received and Release Fund</button>  
                                    <?php 
                                        $disputeExistResult = mysqli_query($connect, "SELECT * from dispute WHERE AuctionID = '".$prodRow['auctionID']."'");
                                        $disputeExist = mysqli_fetch_assoc($disputeExistResult);
                                        if(mysqli_num_rows($disputeExistResult) > 0)
                                        {
                                            ?>
                                            <button class="btn btn-view-dispute" value="View Dispute" type="button" onclick="enterDisputeModal('<?php echo $prodRow['auctionID']; ?>')" >View Dispute</button>
                                            <?php
                                        }
                                        else
                                        {
                                            ?>
                                            <button class="btn btn-raise-dispute" value="Dispute" type="button" onclick="openDisputeModal('<?php echo $prodRow['auctionID']; ?>')" >Raise Dispute</button>                        
                                            <?php
                                        }
                                    ?>
                                </div>
                                <!-- Delivery Address Modal -->
                                <div class="modal fade" id="deliveryAddressModal-<?php echo $prodRow['auctionID']; ?>" tabindex="-1" role="dialog" aria-labelledby="deliveryAddressModalLabel-<?php echo $prodRow['auctionID']; ?>" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="deliveryAddressModalLabel">Enter Delivery Address</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <?php 
                                                    $sqlDeliveryAddress = "SELECT * from delivery where auctionID = '".$row['AuctionID']."'";
                                                    $resultDeliveryAddress = mysqli_query($connect, $sqlDeliveryAddress);
                                                    
                                                    while ($rowDeliveryAddress = mysqli_fetch_assoc($resultDeliveryAddress))
                                                    {
                                                ?>
                                                <!-- Form for delivery address -->
                                                <form id="deliveryAddressForm">
                                                    <div class="form-group">
                                                        <label for="address">Address</label>
                                                        <textarea class="form-control" id="address" rows="3" value="<?php echo $rowDeliveryAddress['address']; ?>" readonly ><?php echo $rowDeliveryAddress['address']; ?></textarea>
                                                    </div>
                                                    <!-- Add more fields as needed -->
                                                </form>
                                                <?php } ?> 
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Dispute Modal -->
                                <div class="modal fade" id="disputeModal-<?php echo $prodRow['auctionID']; ?>" tabindex="-1" role="dialog" aria-labelledby="disputeModalLabel-<?php echo $prodRow['auctionID']; ?>" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="disputeModalLabel">Enter Dispute Description</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <!-- Form for dispute -->
                                                <form id="disputeForm">
                                                    <div class="form-group">
                                                        <label for="description">Dispute Description: </label>
                                                        <textarea class="form-control" id="description" rows="3" ></textarea>
                                                    </div>
                                                    <!-- Add more fields as needed -->
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary" onclick="submitDispute('<?php echo $prodRow['auctionID']; ?>')">Submit Dispute</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- View Dispute Modal -->
                                <div class="modal fade" id="viewDisputeModal-<?php echo $prodRow['auctionID']; ?>" tabindex="-1" role="dialog" aria-labelledby="disputeModalLabel-<?php echo $prodRow['auctionID']; ?>" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="disputeModalLabel">Dispute Modal</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <?php 
                                                    $sqlDispute = "SELECT * from dispute where disputeID = '".$disputeExist['DisputeID']."'";
                                                    $resultDispute = mysqli_query($connect, $sqlDispute);
                                                    
                                                    while ($rowDispute = mysqli_fetch_assoc($resultDispute))
                                                    {
                                                ?>
                                                <!-- Form for delivery address -->
                                                <form id="deliveryAddressForm">
                                                    <div class="form-group">
                                                        <label for="address">Description</label>
                                                        <textarea class="form-control" id="address" rows="3" value="<?php echo $rowDispute['Description']; ?>" readonly ><?php echo $rowDispute['Description']; ?></textarea>
                                                    </div>
                                                    <!-- Add more fields as needed -->
                                                </form>
                                                <?php } ?> 
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <br />
                            <br />
                            <br />
                            <?php }
                            //fetch all completed orders 
                            $sql = "SELECT * from auction WHERE winner_userid = '".$_SESSION["userID"]."' AND status = 'completed' AND (CONVERT_TZ(NOW(), @@session.time_zone, '+08:00') > EndTime)";
                            $result = mysqli_query($connect, $sql);                            
                            echo "<h3><i class='icon anm anm-user-al'></i> Past Orders <a class='text-white text-decoration-underline' data-toggle='collapse'></a></h3>";
                            while ($row = mysqli_fetch_assoc($result))
                            {
                            ?>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div class="your-order-payment">
                                    <div class="your-order">
                                        <h2 class="order-title mb-4">Auction <?php echo $row['auctionID']; ?> [Status: <?php echo $row['status']; ?>]</h2>

                                        <div class="table-responsive-sm order-table"> 
                                            <table id="cartTable" class="bg-white table table-bordered table-hover text-center">
                                                <thead>
                                                    <tr>
                                                        <th class="text-left">Product Name</th>
                                                        <th>Front Image</th>
                                                        <th>Back Image</th>
                                                        <th>Condition</th>
                                                        <th>Description</th>
                                                        <th>Auction Start DateTime</th>
                                                        <th>Auction End DateTime</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $sqll = "SELECT auctionID, ItemName, product_front_image, product_back_image, product_condition, description, StartTime, EndTime, reserved_price, highest_bid_bidid, auction_contract_address FROM auction WHERE AuctionID = '".$row['AuctionID']."'";
                                                        $resultt = mysqli_query($connect, $sqll);
                                                        
                                                        while ($row2 = mysqli_fetch_assoc($resultt))
                                                        {

                                                            $prodResult = mysqli_query($connect, "SELECT auctionID, ItemName, product_front_image, product_back_image, product_condition, description, StartTime, EndTime, reserved_price, highest_bid_bidid, auction_contract_address FROM auction WHERE AuctionID = '".$row['AuctionID']."'");
                                                            $prodRow = mysqli_fetch_assoc($prodResult);
                                                    ?>
                                                    <tr>
                                                        <td class="text-left"><a href="<?php printf('%s?auctionID=%s', 'product-layout.php',  $prodRow['auctionID']); ?>"><?php echo $prodRow['ItemName'];  ?></a><input name="auction_contract_address" class="auction_contract_address" type="hidden" data-auction-id="<?php echo $prodRow['auctionID']; ?>" value="<?php echo $prodRow['auction_contract_address']; ?>"></td>
                                                        <td><img src="./assets/images/productimg/<?php echo $prodRow['product_front_image']; ?>" width="100" height="100"></td>
                                                        <td><img src="./assets/images/productimg/<?php echo $prodRow['product_back_image']; ?>" width="100" height="100"></td>
                                                        <td><?php echo $row2['product_condition']; ?></td>
                                                        <td><?php echo $row2['description']; ?></td>
                                                        <td><?php echo $prodRow['StartTime']; ?></td>
                                                        <td><?php echo $prodRow['EndTime']; ?></td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                                <tfoot class="font-weight-600">
                                                    <tr>
                                                        <!--
                                                        <td colspan="4" class="text-right">Shipping </td>
                                                        <td>RM 50.00</td>
                                                    </tr>
                                                        -->
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>          
                                </div>
                                <div class="order-button-payment">
                                    <button class="btn btn-enter-delivery" value="View Delivery Address" type="button" onclick="enterDeliveryAddressModal('<?php echo $prodRow['auctionID']; ?>')" >View Delivery Address</button>
                                </div>
                                <!-- Delivery Address Modal -->
                                <div class="modal fade" id="deliveryAddressModal-<?php echo $prodRow['auctionID']; ?>" tabindex="-1" role="dialog" aria-labelledby="deliveryAddressModalLabel-<?php echo $prodRow['auctionID']; ?>" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="deliveryAddressModalLabel">Enter Delivery Address</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <?php 
                                                    $sqlDeliveryAddress = "SELECT * from delivery where auctionID = '".$row['AuctionID']."'";
                                                    $resultDeliveryAddress = mysqli_query($connect, $sqlDeliveryAddress);
                                                    
                                                    while ($rowDeliveryAddress = mysqli_fetch_assoc($resultDeliveryAddress))
                                                    {
                                                ?>
                                                <!-- Form for delivery address -->
                                                <form id="deliveryAddressForm">
                                                    <div class="form-group">
                                                        <label for="address">Address</label>
                                                        <textarea class="form-control" id="address" rows="3" value="<?php echo $rowDeliveryAddress['address']; ?>" readonly ><?php echo $rowDeliveryAddress['address']; ?></textarea>
                                                    </div>
                                                    <!-- Add more fields as needed -->
                                                </form>
                                                <?php } ?> 
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                            <br />
                            <?php }
                        }
                        else{
                           echo "<h3><i class='icon anm anm-user-al'></i> Please login to view order. <a href='login.php' id='customer' class='text-white text-decoration-underline' >Click here to login</a></h3>";
                        }
                      ?>
                        
                    </div>
                </div>
        </div>   
</div>
    <!--End Body Content-->
    <!-- JavaScript files-->
    <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="js/front.js"></script>
        <!-- Including Jquery -->
        <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
        <script src="assets/js/vendor/jquery.cookie.js"></script>
        <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
        <script src="assets/js/vendor/wow.min.js"></script>
        <!-- Including Javascript -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/lazysizes.js"></script>
        <script src="assets/js/main.js"></script>
        <!-- Javascript functions -->
        <script>

            let web3 = new Web3(window.ethereum);
            let auctionContract, auctionManagerContract; // Declare the contract variable here

            // Fetch contract data and initialize AuctionManager contract
            function fetchAuctionManagerContractData() {
                fetch('../build/contracts/AuctionManager.json')
                .then(function(response) {
                    return response.json();
                })
                .then(function(data) {
                    const abi = data.abi;
                    const networkId = '5777'; // Replace with the network ID you're using
                    const contractAddress = data.networks[networkId].address;

                    // Initialize the contract
                    auctionManagerContract = new web3.eth.Contract(abi, contractAddress);
                    
                    // Debug logs
                    console.log("ABI:", abi);
                    console.log("Contract Address:", contractAddress);
                    console.log(auctionManagerContract.methods);
                })
                .catch(function(error) {
                    console.error('Error fetching contract data:', error);
                });
            }    

            // Fetch contract data and initialize Auction contract
            function fetchAuctionContractData() {
            fetch('../build/contracts/Auction.json')
                .then(function(response) {
                return response.json();
                })
                .then(function(data) {
                const abi = data.abi;
                const networkId = '5777'; // Replace with the network ID you're using
                const contractAddress = data.networks[networkId].address;

                // Initialize the contract
                auctionContract = new web3.eth.Contract(abi, contractAddress);
                
                // Debug logs
                console.log("ABI:", abi);
                console.log("Contract Address:", contractAddress);
                console.log(auctionContract.methods);
                })
                .catch(function(error) {
                console.error('Error fetching contract data:', error);
                });
            }

            // Call the function to fetch contract data
            fetchAuctionContractData();
            fetchAuctionManagerContractData();

            async function requestAccount() {
                const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
                return accounts[0];
            }

            function enterDeliveryAddressModal(auctionID) {
                // Use the auctionID to target the correct modal
                console.log("open delivery modal for: " + auctionID);
                $('#deliveryAddressModal-' + auctionID).modal('show');
            }

            async function releaseFund(auctionID) {
                console.log("hi release fund: " + auctionID);
                const auctionAddressInput = document.querySelector('.auction_contract_address[data-auction-id="' + auctionID + '"]');
                const auctionAddress = auctionAddressInput.value;
                console.log('auction Address: ' + auctionAddress);

                try {
                    const account = await requestAccount();
                    // Define the gas limit and gas price
                    const gasLimit = web3.utils.toHex(3000000); // Example gas limit
                    const gasPrice = web3.utils.toHex(web3.utils.toWei('10', 'gwei')); // Example gas price

                    const receipt = await auctionManagerContract.methods.releaseAuctionFund(auctionAddress).send({
                        from: account,
                        gas: gasLimit,
                        gasPrice: gasPrice
                    });
                    // Get the transaction hash
                    const transactionHash = receipt.transactionHash;
                    const releaseFundEvent = receipt.events.ReleaseFund; // Assuming there's an event for funds release
                    console.log("Transaction Hash:", transactionHash);
                    console.log("Receipt:", receipt);
                    console.log("releaseFundEvent:", releaseFundEvent);
                    console.log("Funds released successfully");
                    alert('Funds released successfully.');
                    if (releaseFundEvent) {
                        // TODO: Perform AJAX request to send the address to the server
                        try {
                            // 1. change delivery status = completed
                            // 2. change escrow status = completed
                            // 3. change auction status = completed 

                            // Data to send to PHP
                            let formData = new FormData();
                            formData.append('auctionID', auctionID);
                            formData.append('auction_contract_address', auctionAddress);
                            formData.append('transactionHash', transactionHash);
                            
                            // Send data to PHP script
                            const response = await $.ajax({
                                url: 'completeDeliveryEscrowDatabase.php',
                                type: 'POST',
                                data: formData, 
                                contentType: false, // Do not set content type header
                                processData: false, // Do not process data
                                success: function(response) {
                                    console.log("Success:", response);
                                    alert('Delivery and escrow completed successfully.');
                                    location.reload();
                                },
                                error: function(xhr, status, error) {
                                    console.error("Error:", error);
                                    alert('Delivery and escrow failed. ' + error );
                                    location.reload();
                                }
                            });
                        } catch (error) {
                            console.error('Error sending data to PHP:', error);
                        }
                    }
                    else {
                        console.log("releaseFundEvent is empty" + releaseFundEvent);
                    }
                } catch (error) {
                    console.error("Error releasing funds:", error);
                    alert('Failed to release funds: ' + error.message);
                }
            }

            function enterDisputeModal(auctionID) {
                // Use the auctionID to target the correct modal
                console.log("open dispute modal for: " + auctionID);
                $('#viewDisputeModal-' + auctionID).modal('show');
            }

            function openDisputeModal(auctionID) {
                // Use the auctionID to target the correct modal
                console.log("open dispute modal for: " + auctionID);
                $('#disputeModal-' + auctionID).modal('show');
            }

            async function submitDispute(auctionID) {
                console.log("hi submit dispute: " + auctionID);
                const auctionAddressInput = document.querySelector('.auction_contract_address[data-auction-id="' + auctionID + '"]');
                const auctionAddress = auctionAddressInput.value;
                console.log('auction Address: ' + auctionAddress);
                // Get the address value from the correct modal
                var description = document.querySelector('#disputeModal-' + auctionID + ' #description').value;
                console.log('dispute description: ' + description);

                try {
                    // Data to send to PHP
                    let formData = new FormData();
                    formData.append('auctionID', auctionID);
                    formData.append('auction_contract_address', auctionAddress);
                    formData.append('description', description);
                    
                    // Send data to PHP script
                    const response = await $.ajax({
                        url: 'addDisputeDatabase.php',
                        type: 'POST',
                        data: formData, 
                        contentType: false, // Do not set content type header
                        processData: false, // Do not process data
                        success: function(response) {
                            console.log("Success:", response);
                            alert('Dispute added successfully.');
                            location.reload();
                        },
                        error: function(xhr, status, error) {
                            console.error("Error:", error);
                            alert('Adding dispute failed. ' + error );
                            location.reload();
                        }
                    });
                } catch (error) {
                    console.error('Error sending data to PHP:', error);
                }
            }
    
    </script>
</script>
<?php
    include("footer.php");
?>

