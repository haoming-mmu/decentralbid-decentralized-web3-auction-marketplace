<?php
    include_once 'header.php';
    include ('dataconnection.php');
?>

<?php																																																													

    $sql = "SELECT auctionID, ItemName, StartTime, duration, EndTime, product_condition, description, reserved_price, category_id, SellerUserID, product_front_image, product_back_image from auction";
    $result = mysqli_query($connect, $sql);	
 ?>

    <!--End Header-->
    <!--Body Content-->
    <div id="page-content">
        <!--Body Content-->
        <!--Featured Product-->
        <div class="product-rows section">
        	<div class="container">
            	<div class="row">
                	<div class="col-12 col-sm-12 col-md-12 col-lg-12">
        				      <div class="section-header text-center">
                            <h2 class="h2">Featured Auctions</h2>
                            <p>All of our ongoing auctions</p>
                      </div>
            		  </div>
              </div>
                <div class="grid-products">
	                <div class="row">
                  <?php 
                  	if(isset($_POST['searchinput']))
                      {
                          $searchinput = $_POST['searchinput'];
                          $check = mysqli_query($connect,"select auctionID, ItemName, StartTime, duration, EndTime, product_condition, description, reserved_price, category_id, SellerUserID, product_front_image, product_back_image from auction where ItemName like '%$searchinput%'");
                          $checkrows=mysqli_num_rows($check);
                          if($checkrows>0) 
                          {
                            while($row = mysqli_fetch_array($check)){?>
                            <div class="col-6 col-sm-6 col-md-4 col-lg-4 item grid-view-item style2">
                        	<div class="grid-view_image">
                                <!-- start product image -->
                                <a href="<?php printf('%s?auctionID=%s', 'product-layout.php',  $row['auctionID']); ?>" class="grid-view-item__link">
                                    <!-- image -->
                                    <img class="primary" data-src="./assets/images/productimg/<?php echo $row["product_front_image"]; ?>" src="./assets/images/productimg/<?php echo $row["product_front_image"]; ?>" alt="" title="product">
                                    <!-- End image -->
                                    <!-- Hover image -->
                                    <img class="hover lazyload" data-src="./assets/images/productimg/<?php echo $row["product_back_image"]; ?>" src="./assets/images/productimg/<?php echo $row["product_back_image"]; ?>" alt="" title="product">
                                    <!-- End hover image -->
                                    <!-- product label -->
                                    <!-- End product label -->
                                </a>
                                <!-- end product image -->
                                <!--start product details -->
                                <div class="product-details hoverDetails text-center mobile">
                                    <!-- product name -->
                                    <div class="product-name">
                                        <a href="<?php printf('%s?auctionID=%s', 'product-layout.php',  $row['auctionID']); ?>"><?php echo $row['ItemName']; ?></a>
                                    </div>
                                    <!-- End product name -->
                                    <!-- product price -->
                                    <div class="product-price">
                                        <!--<span class="old-price">$500.00</span>-->
                                        <span class="price">Reserved Price: <?php echo $row['reserved_price']; ?> ETH </span>
                                    </div>
                                    <div class="product-price">
                                        <!--<span class="old-price">$500.00</span>-->
                                        <span class="price">Start DateTime: <?php echo $row['StartTime']; ?> UTC+8</span>
                                    </div>
                                    <div class="product-price">
                                        <!--<span class="old-price">$500.00</span>-->
                                        <span class="price">End DateTime: <?php echo $row['EndTime']; ?> UTC+8</span>
                                    </div>
                                    <!-- End product price -->
                                  </div>
                                <!-- End product details -->
                            </div>
                        </div>
                        <?php
                          }}
                          else 
                          {
                  ?>
                              <script type="text/javascript">
                                  Swal.fire({
                                          icon: 'error',
                                          title: 'Oops...',
                                          text: 'No records.',
                                          })
                              </script>
                  <?php	
                          }
                      }
                      else
                      {
                          while ($row = mysqli_fetch_array($result)){?>

                        <div class="col-6 col-sm-6 col-md-4 col-lg-4 item grid-view-item style2">
                        	<div class="grid-view_image">
                                <!-- start product image -->
                                <a href="<?php printf('%s?auctionID=%s', 'product-layout.php',  $row['auctionID']); ?>" class="grid-view-item__link">
                                    <!-- image -->
                                    <img class="primary primary img-fluid" data-src="./assets/images/productimg/<?php echo $row["product_front_image"]; ?>" src="./assets/images/productimg/<?php echo $row["product_front_image"]; ?>" alt="<?php echo $row["product_front_image"]; ?>" title="product">
                                    <!-- End image -->
                                    <!-- Hover image -->
                                    <img class="hover primary img-fluid lazyload" data-src="./assets/images/productimg/<?php echo $row["product_back_image"]; ?>" src="./assets/images/productimg/<?php echo $row["product_back_image"]; ?>" alt="<?php echo $row["product_back_image"]; ?>" title="product">
                                    <!-- End hover image -->
                                    <!-- product label -->
                                    <!-- End product label -->
                                </a>
                                <!-- end product image -->
                                <!--start product details -->
                                <div class="product-details hoverDetails text-center mobile">
                                    <!-- product name -->
                                    <div class="product-name">
                                        <a href="<?php printf('%s?auctionID=%s', 'product-layout.php',  $row['auctionID']); ?>"><?php echo $row['ItemName']; ?></a>
                                    </div>
                                    <!-- End product name -->
                                    <!-- product price -->
                                    <div class="product-price">
                                        <!--<span class="old-price">$500.00</span>-->
                                        <span class="price">Reserved Price: <?php echo $row['reserved_price']; ?> ETH </span>
                                    </div>
                                    <div class="product-price">
                                        <!--<span class="old-price">$500.00</span>-->
                                        <span class="price">Start DateTime: <?php echo $row['StartTime']; ?> UTC+8</span>
                                    </div>
                                    <div class="product-price">
                                        <!--<span class="old-price">$500.00</span>-->
                                        <span class="price">End DateTime: <?php echo $row['EndTime']; ?> UTC+8</span>
                                    </div>
                                    <!-- End product price -->
                                  </div>
                                <!-- End product details -->
                            </div>
                        </div>
                  <?php }} ?>
                	</div>
                </div>
           </div>
        </div>	
        <!--End Featured Product-->
                  </div>
</body>

</html>

<?php

include("footer.php");

?>