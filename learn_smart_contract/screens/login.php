<div class="loginheader">
    <?php
        include_once 'header.php';
    ?>
</div>
    <!--Body Content-->
    <div id="page-content">
    	<!--Page Title-->
    	<div class="page section-header text-center">
			<div class="page-title">
        		<div class="wrapper"><h1 class="page-width">Login</h1></div>
      		</div>
		</div>
        <!--End Page Title-->
        
        <div class="container page-content-wrapper">
        	<div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 main-col offset-md-3">
                	<div class="mb-4">
                     <button id="signin-btn" onclick="signIn()">Sign In with Ethereum Wallet</button>
                    </div>
               	</div>
            </div>
        </div>
        
    </div>
    <!--End Body Content-->

    <footer class="sticky-footer">
        <?php
            include_once 'footer.php';
        ?>
    </footer>
    
    <!--Scoll Top-->
    <span id="site-scroll"><i class="icon anm anm-angle-up-r"></i></span>
    <!--End Scoll Top-->
    
     <!-- Including Jquery -->
     <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
     <script src="assets/js/vendor/jquery.cookie.js"></script>
     <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
     <script src="assets/js/vendor/wow.min.js"></script>
     <!-- Including Javascript -->
     <script src="assets/js/bootstrap.min.js"></script>
     <script src="assets/js/plugins.js"></script>
     <script src="assets/js/popper.min.js"></script>
     <script src="assets/js/lazysizes.js"></script>
     <script src="assets/js/main.js"></script>
</div>
<script>
    let web3 = new Web3(window.ethereum);
    if (web3) {
    console.log("Web3 initialized", web3);
    } else {
        console.error("Failed to initialize Web3");
    }

    console.log("Web3.js version:", Web3.version); // Check Web3.js version
    console.log("Web3 object:", web3); // Check if the Web3 object exists
    console.log("Ethereum provider:", window.ethereum); // Check if the Ethereum provider exists
    console.log("Personal API available:", web3.eth.personal); // Check if the personal API is available

    const message = "Please sign this message to confirm your identity.";

    async function requestAccount() {
    const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
    return accounts[0];
    }
    async function signIn() {
    try {
        const account = await requestAccount();
        const signature = await web3.eth.personal.sign(message, account, '');

        const isVerified = await verifySignature(account, signature);
        if (isVerified) {
            // First AJAX request
            const response = await $.ajax({
                type: 'POST',
                url: 'start_session_customer.php',
                data: { account: account }
            });
            console.log(response); 

            // Second AJAX request
            const response2 = await $.ajax({
                url: 'wallet_address_check.php',
                type: 'POST',
                data: {} 
            });
            console.log("Second call was successful", response2);

            // Fetch account and balance
            const balanceWei = await web3.eth.getBalance(account);
            const balanceEther = web3.utils.fromWei(balanceWei, 'ether');
            const accountInfo = `Connected account: ${account}<br>Balance: ${balanceEther} ETH`;
            console.log(accountInfo);

            alert('Successfully signed in!');
            window.location = 'home.php';
        } else {
            alert('Failed to sign in!');
        }
    } catch (error) {
        console.error(error);
        alert('Error signing in: ' + error.message);
    }
    }
    // async function signIn() {
    //     try {
    //         const account = await requestAccount();
    //         const signature = await web3.eth.personal.sign(message, account, '');

    //         const isVerified = await verifySignature(account, signature);
    //         if (isVerified) 
    //         {
    //             // Make an AJAX request to start the PHP session
    //             $.ajax({
    //                 type: 'POST',
    //                 url: 'start_session_customer.php',
    //                 data: { account: account },
    //                 success: function(response) {
    //                     console.log(response); // Log the server's response
    //                     $.ajax({
    //                         url: 'wallet_address_check.php',
    //                         type: 'POST',
    //                         data: {

    //                         },
    //                         success: function(response2) {
    //                             // Process response2
    //                             console.log("Second call was successful", response2);
    //                             alert('Successfully signed in!');
    //                             window.location = 'home.php';
    //                         },
    //                         error: function(error) {
    //                             console.error("Error in second call", error);
    //                         }
    //                     });

    //                 },
    //                 error: function(error) {
    //                     console.error('Error starting session:', error);
    //                 }
    //             });
    //         } else {
    //             alert('Failed to sign in!');
    //         }
    //     } catch (error) {
    //         console.error(error);
    //         alert('Error signing in: ' + error.message);
    //     }
    // }

    async function verifySignature(account, signature) {
      const signer = await web3.eth.personal.ecRecover(message, signature);
      return signer.toLowerCase() === account.toLowerCase();
    }

</script>
</body>
</html>