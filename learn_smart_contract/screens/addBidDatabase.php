<?php 
    include ('dataconnection.php');
    session_start();
    	
    echo "<script>console.log('Hi from addBidDatabase.php');</script>";		 
    $auction_id = $_POST["auctionID"]; 
    $amount = $_POST["amount"]; 
    $timePlaced = $_POST["timePlaced"]; 
    $transactionHash = $_POST["transactionHash"];
    $auction_contract_address = $_POST["auction_contract_address"]; 
    
    echo "<script>console.log('Auction Objects: $auction_id + $amount + $timePlaced + $transactionHash + $auction_contract_address');</script>";		
    
    $Index = 1;
    $bid_id = sprintf("B%04d", $Index);
    $idCheckSQL = "SELECT bidID from bid ORDER BY bidID";
    $idQuery = mysqli_query($connect, $idCheckSQL); 

    while ($idResult = mysqli_fetch_assoc($idQuery)) {
        if($idResult['bidID'] == $bid_id)
        {
            $Index += 1;
            $bid_id = sprintf("B%04d", $Index);
        }
    }

    $query = "INSERT INTO bid (bidID, amount, timePlaced, transactionHash, auction_contract_address, AuctionID, BuyerUserID) values ('$bid_id', '$amount', '$timePlaced', '$transactionHash', '$auction_contract_address', '$auction_id', '{$_SESSION['userID']}')";																																																													

    if(mysqli_query($connect, $query)) {
        ?>
        <script>
            Swal.fire('<?= $bid_id ?>', 'added successfully', 'success');
        </script>

        <?php
        if(mysqli_affected_rows($connect) >0)
        {
            $reply = 0;
            $status = "success";
            $response = " Add New Bid Successfully. (added to Bid table).";
        }
        else
        {
            $reply = 1;
            $status = "failed"; 
            $response = "Add New Bid Unsuccessful.";   
        }
    } else {
        ?>
        <script type="text/javascript">
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'Place bid failed.' });
            console.log('Error: <?= mysqli_error($connect); ?>');
        </script>
        <?php
    }
?>