<?php
    include_once 'header.php';
    include ('dataconnection.php');
?>

<?php


    $auctionID= $_GET['auctionID'] ?? 1;
    //foreach ($product->getData1() as $item1) :
    //if($item1['fk_category_id'] == $product_name) :
    
    $sql = "SELECT auctionID, ItemName, StartTime, duration, EndTime, product_condition, description, reserved_price, category_id, SellerUserID, product_front_image, product_back_image, auction_contract_address from auction  WHERE AuctionID ='$auctionID'";
    $result = mysqli_query($connect, $sql);
    
?>


    <!--End Header-->
        
        <!--Body Content-->
        <div id="page-content">
            <!--MainContent-->
            <div id="MainContent" class="main-content" role="main">
                <!--Breadcrumb-->
                <div class="bredcrumbWrap">
                    <div class="container breadcrumbs">
                        <a href="home.php" title="Back to the home page">Home</a><span aria-hidden="true">›</span><span>Auction Detail</span>
                    </div>
                </div>
                <!--End Breadcrumb-->
                
                <div id="ProductSection-product-template" class="product-template__container prstyle1 container">
                    <!--product-single-->
                    <div class="product-single">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <?php
                                if(isset($_SESSION["userID"])) 
                                {
                            ?>
                                <div class="product-details-img">
                                    <div class="product-thumb">
                                        <div id="gallery" class="product-dec-slider-2 product-tab-left">
                                        <?php
                                          $stock=0;
                                           $num = '-4';
                                            while ($item1 = mysqli_fetch_array($result))
                                            {
                                              $ItemName = $item1["ItemName"];
                                              $StartTime = $item1["StartTime"];
                                              $duration = $item1["duration"];
                                              $EndTime = $item1["EndTime"];
                                              $product_condition = $item1["product_condition"];
                                              $description = $item1["description"];
                                              $reserved_price = $item1["reserved_price"];
                                              $category_id = $item1["category_id"];
                                              $SellerUserID = $item1["SellerUserID"];
                                              $product_front_image = $item1["product_front_image"];
                                              $product_back_image = $item1["product_back_image"];
                                              $auction_contract_address = $item1["auction_contract_address"];
                                              // Convert $EndTime from UTC+0 to UTC+7
                                              $endTimeTimestampUtc8 = strtotime($EndTime) - 25200; // Minus 7 hours (in seconds)

                                              // Get current time in UTC+8
                                              $currentTimeUtc8 = time();

                                              // Compare current time with adjusted end time
                                              $isAuctionOver = $currentTimeUtc8 > $endTimeTimestampUtc8; // true if current time is after adjusted $EndTime
                                              
                                              //check if the user already placed bid in this auction
                                              $sqlCheckBidExist = "SELECT * from bid WHERE BuyerUserID = '".$_SESSION['userID']."' and auction_contract_address = '$auction_contract_address'"; 
                                              $resultCheckBidExist = mysqli_query($connect, $sqlCheckBidExist);
                                              if (mysqli_num_rows($resultCheckBidExist)==0) 
                                              {
                                                $bidExist = "noExist";
                                              }
                                              else 
                                              {
                                                $bidExist = "exist";
                                              }

                                         ?>   
                                            <a data-image="./assets/images/productimg/<?php echo $item1["product_front_image"]; ?>" class="slick-slide slick-cloned" data-slick-index="<?php echo $num; ?>" aria-hidden="true" tabindex="-1">
                                                <img class="blur-up lazyload" data-src="./assets/images/productimg/<?php echo $item1["product_front_image"]; ?>" src="./assets/images/productimg/<?php echo $item1["product_front_image"];?>" alt="" />
                                            </a>
                                            <a data-image="./assets/images/productimg/<?php echo $item1["product_back_image"]; ?>" class="slick-slide slick-cloned" data-slick-index="<?php echo $num; ?>" aria-hidden="true" tabindex="-1">
                                                <img class="blur-up lazyload" data-src="./assets/images/productimg/<?php echo $item1["product_back_image"]; ?>" src="./assets/images/productimg/<?php echo $item1["product_back_image"];?>" alt="" />
                                            </a>

                                             <?php
                                                $num++;
                                            ?>
                                         
                                        </div>
                                    </div>
                                    <div class="zoompro-wrap product-zoom-right pl-20">
                                        <div class="zoompro-span">
                                            <img class="blur-up lazyload zoompro" data-zoom-image="./assets/images/productimg/<?php echo $item1["product_front_image"]; ?>" alt="" src="./assets/images/productimg/<?php echo $item1["product_front_image"];?>" />
                                        </div>
                                        <div class="product-buttons">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="product-single__meta">
                                        <h1 class="product-single__title">Product Name: <?php echo $item1["ItemName"]; ?></h1>
                                        <div class="product-nav clearfix">					
                                        </div>
                                        <div class="prInfoRow">
                                      
                                            <div class="product-stock" id="inStock"> </div>
                                        </div>
                                        <p class="product-single__price product-single__price-product-template">
                                            <span class="hidden">Reserved Price: <?php echo $item1["reserved_price"]; ?></span>
                                            <span class="product-price__price product-price__price-product-template product-price__sale product-price__sale--single">
                                            <?php
                                                $categorySql = "SELECT category_name from category WHERE category_id ='$category_id'";
                                                $categoryResult = mysqli_query($connect, $categorySql);
                                                $item2 = mysqli_fetch_array($categoryResult);
                                                $category =  $item2[0];
                                             ?>
                                            <span id="ProductPrice-product-template">Category: <?php echo $category; ?><span class="money"></span></span>
                                              <br/>
                                            <span id="ProductPrice-product-template">Condition: <?php echo $product_condition; ?><span class="money"></span></span>
                                              <br/>
                                            <span id="ProductPrice-product-template">Description: <?php echo $description; ?><span class="money"></span></span> 
                                              <br/>
                                            <span id="ProductPrice-product-template"><span class="money">Reserved Price: <?php echo $reserved_price;?>

                                              </span>
                                            </span>
                                          </span>
                                            
                                        </p>
                                       <!--...-->
           
                                    <form method="post" id="bid_form" accept-charset="UTF-8" class="product-form product-form-product-template hidedropdown" enctype="multipart/form-data">
                                        <div class="swatch clearfix swatch-0 option1" data-option-index="0">
                                            <div class="product-form__item">
                                            
                                            <!--hidden input-->
                                            
                                             <input name="userID" id="userID" type="hidden" value="<?php echo $_SESSION['userID'] ; ?>"></input>
                                             <input name="userWalletAddress" id="userWalletAddress" type="hidden" value="<?php echo $_SESSION['user_wallet_address'] ; ?>"></input>
                                             <input name="auctionID" id="auctionID" type="hidden" value="<?php echo $auctionID ; ?>"></input>
                                             <input name="ItemName" id="itemName" type="hidden" value="<?php echo $ItemName ; ?>"></input>
                                             <input name="StartTime" id="startTime" type="hidden" value="<?php echo $StartTime ; ?>"></input>
                                             <input name="duration" id="duration" type="hidden" value="<?php echo $duration ; ?>"></input>
                                             <input name="EndTime" id="endTime" type="hidden" value="<?php echo $EndTime ; ?>"></input>
                                             <input name="product_condition" id="product_condition" type="hidden" value="<?php echo $product_condition ; ?>"></input>
                                             <input name="description" id="description" type="hidden" value="<?php echo $description ; ?>"></input>
                                             <input name="reserved_price" id="reserved_price" type="hidden" value="<?php echo $reserved_price ; ?>"></input>
                                             <input name="category_id" id="category_id" type="hidden" value="<?php echo $category_id ; ?>"></input>
                                             <input name="SellerUserID" id="sellerUserID" type="hidden" value="<?php echo $SellerUserID ; ?>"></input>
                                             <input name="product_front_image" id="product_front_image" type="hidden" value="<?php echo $product_front_image ; ?>"></input>
                                             <input name="product_back_image" id="product_back_image" type="hidden" value="<?php echo $product_back_image ; ?>"></input>
                                             <input name="auction_contract_address" id="auction_contract_address" type="hidden" value="<?php echo $auction_contract_address ; ?>"></input>
                                             <input name="bidExist" id="bidExist" type="hidden" value="<?php echo $bidExist ; ?>"></input>
                                             <?php
                                                /*$sql = "SELECT fk_category_id from product WHERE product_name ='$product_name'";
                                                $result = mysqli_query($connect, $sql);
                                                $item2 = mysqli_fetch_array($result);
                                                $category =  $item2[0]; */
                                             ?>
                                             <input name="product_category" type="hidden" value="<?php echo $category ; ?>"></input>
                                             <!-- end hidden input -->
                                            
                                             <?php
                                                $walletSql = "SELECT WalletAddress from customer WHERE UserID ='$SellerUserID'";
                                                $walletResult = mysqli_query($connect, $walletSql);
                                                $item3 = mysqli_fetch_array($walletResult);
                                                $walletAddress =  $item3[0];
                                             ?>
                                             <!-- <label class="header">Color: <span class="slVariant"><?php //echo $item1["fk_colour_id"];?></span></label> -->
                                             <label class="header">Bid Start Time: <span class="slVariant"><?php echo $StartTime;?></span></label>
                                             <label class="header">Bid End Time: <span class="slVariant"><?php echo $EndTime;?></span></label>
                                             <label class="header">Duration: <span class="slVariant"><?php echo $duration;?></span></label>
                                             <label class="header">Reserve Price: <span class="slVariant"><?php echo $reserved_price;?></span></label>
                                             <label class="header">Auctioneer: <span class="slVariant"><?php echo $SellerUserID;?></span></label>
                                             <label class="header">Auctioneer Wallet Address: <span class="slVariant"><?php echo $walletAddress;?></span></label>
                                            
                                             <?php
                                            }

                                            ?>
                                            </div>
                                        </div>
                                        <!-- Product Action -->
                                        <div class="product-action clearfix">
                                            <div class="product-form__item--quantity">
                                                <div class="wrapQtyBtn">
                                                    <div class="qtyField">
                                                        <input style="border: 1px solid grey;" name="bid" id="bid" type="text" pattern="^\d+(\.\d{2})?$" required placeholder="Bid Amount (eth)" <?php echo $isAuctionOver ? 'disabled' : ''; ?>  onkeypress="return (function(event) {
                                                            var charCode = (event.which) ? event.which : event.keyCode;

                                                            // Allow decimal point
                                                            if (charCode == 46) {
                                                                // Check if the text already contains a decimal point
                                                                if (event.target.value.indexOf('.') !== -1) {
                                                                    return false;
                                                                }
                                                            } else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                                                                // Block non-numerical input
                                                                return false;
                                                            }

                                                            // Check for more than two digits after the decimal point
                                                            var parts = event.target.value.split('.');
                                                            if (parts.length > 1 && parts[1].length >= 2) {
                                                                return false;
                                                            }

                                                            return true;
                                                        })(event)">
                                                    </div>
                                                </div>
                                            </div> 
                                            <input type=hidden value="<?=$_SESSION["userid"]; ?>" id='custID'>                               
                                            <div class="product-form__item--submit" style="position: relative; display: inline-block;">
                                                <button type="button" id="placeBidBtn" name="placeBidBtn" class="btn product-form__cart-submit" <?php echo $isAuctionOver ? 'disabled' : ''; ?> onclick="placeBid(event)"> 
                                                    <span>Place Bid</span>
                                                </button>
                                                <?php if ($isAuctionOver): ?>
                                                <div style="position: absolute; top: 0; right: 0; bottom: 0; left: 0; cursor: pointer;"
                                                    onclick="alert('Auction has ended.');"></div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <!-- End Product Action -->
                                    </form>
                                    <div class="display-table shareRow">
                                            <div class="display-table-cell medium-up--one-third">
                                            </div>
                                        </div>
                                </div>
                        </div>
                        <?php 
                        }
                        else{
                           echo "<h3><i class='icon anm anm-user-al'></i> Please login to auction details. <a href='login.php' id='customer' class='text-white text-decoration-underline' >Click here to login</a></h3>";
                        }
                      ?>
                    </div>
                    <!--End-product-single-->
                    
                    <!--Related Product Slider-->

                    <?php
                 //include_once 'product-section-2.php';
                    ?>

                    <!--End Related Product Slider-->
                    
                   
                    </div>
                <!--#ProductSection-product-template-->
            </div>
            <!--MainContent-->
        </div>
    	<!--End Body Content-->
    
    <!--Footer-->
    <?php
    include_once 'footer.php';
?>
    <!--End Footer-->
    <!--Scoll Top-->
    <span id="site-scroll"><i class="icon anm anm-angle-up-r"></i></span>
    <!--End Scoll Top-->
	</div>
    
        
     <!-- Including Jquery -->
     <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
     <script src="assets/js/vendor/jquery.cookie.js"></script>
     <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
     <script src="assets/js/vendor/wow.min.js"></script>
     <!-- Including Javascript -->
     <script src="assets/js/bootstrap.min.js"></script>
     <script src="assets/js/plugins.js"></script>
     <script src="assets/js/popper.min.js"></script>
     <script src="assets/js/lazysizes.js"></script>
     <script src="assets/js/main.js"></script>
     <!-- Photoswipe Gallery -->
     <script src="assets/js/vendor/photoswipe.min.js"></script>
     <script src="assets/js/vendor/photoswipe-ui-default.min.js"></script>
     <script>
        $(function(){
            var $pswp = $('.pswp')[0],
                image = [],
                getItems = function() {
                    var items = [];
                    $('.lightboximages a').each(function() {
                        var $href   = $(this).attr('href'),
                            $size   = $(this).data('size').split('x'),
                            item = {
                                src : $href,
                                w: $size[0],
                                h: $size[1]
                            }
                            items.push(item);
                    });
                    return items;
                }
            var items = getItems();
        
            $.each(items, function(index, value) {
                image[index]     = new Image();
                image[index].src = value['src'];
            });
            $('.prlightbox').on('click', function (event) {
                event.preventDefault();
              
                var $index = $(".active-thumb").parent().attr('data-slick-index');
                $index++;
                $index = $index-1;
        
                var options = {
                    index: $index,
                    bgOpacity: 0.9,
                    showHideOpacity: true
                }
                var lightBox = new PhotoSwipe($pswp, PhotoSwipeUI_Default, items, options);
                lightBox.init();
            });
        });
        let web3 = new Web3(window.ethereum);
        let auctionContract, auctionManagerContract; // Declare the contract variable here

        // Fetch contract data and initialize AuctionManager contract
        function fetchAuctionManagerContractData() {
            fetch('../build/contracts/AuctionManager.json')
            .then(function(response) {
                return response.json();
            })
            .then(function(data) {
                const abi = data.abi;
                const networkId = '5777'; // Replace with the network ID you're using
                const contractAddress = data.networks[networkId].address;

                // Initialize the contract
                auctionManagerContract = new web3.eth.Contract(abi, contractAddress);
                
                // Debug logs
                console.log("ABI:", abi);
                console.log("Contract Address:", contractAddress);
                console.log(auctionManagerContract.methods);
            })
            .catch(function(error) {
                console.error('Error fetching contract data:', error);
            });
        }    

        // Fetch contract data and initialize Auction contract
        function fetchAuctionContractData() {
        fetch('../build/contracts/Auction.json')
            .then(function(response) {
            return response.json();
            })
            .then(function(data) {
            const abi = data.abi;
            const networkId = '5777'; // Replace with the network ID you're using
            const contractAddress = data.networks[networkId].address;

            // Initialize the contract
            auctionContract = new web3.eth.Contract(abi, contractAddress);
            
            // Debug logs
            console.log("ABI:", abi);
            console.log("Contract Address:", contractAddress);
            console.log(auctionContract.methods);
            })
            .catch(function(error) {
            console.error('Error fetching contract data:', error);
            });
        }

        // Call the function to fetch contract data
        fetchAuctionContractData();
        fetchAuctionManagerContractData();

        async function requestAccount() {
            const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
            return accounts[0];
        }

        function handleBidButtonClick(event, isAuctionOver) {
            if (isAuctionOver) {
                alert("Auction has ended.");
            } else {
                placeBid(event); // Call your existing placeBid function
            }
        }

        async function placeBid(event) {
            event.preventDefault();
            //check if bidder is bidding
            var sellerUserID = document.getElementById('sellerUserID').value;
            var userID = document.getElementById('userID').value;
            console.log(sellerUserID);
            console.log(userID);
            if(sellerUserID != userID)
            {
                var bidExist = document.getElementById('bidExist').value;
                console.log('bidExist value:' + bidExist);
                if(bidExist != "exist")
                {
                    var bidAmountCheck = document.getElementById('bid').value;
                    if (!bidAmountCheck ) {
                        alert('Please fill in the bid amount field.');
                        console.log('Bid amount field.');
                        return;
                    }
                    try 
                    {
                        const account = await requestAccount();
                        const bidAmount = document.getElementById('bid').value;
                        let auctionAddress = document.getElementById('auction_contract_address').value; 
                        console.log(account);
                        console.log(bidAmount);
                        console.log(auctionAddress);
                        
                        //mine empty block
                        await mineCurrentTimestampBlock();

                        // Initialize the specific Auction contract instance
                        const specificAuctionContract = new web3.eth.Contract(auctionContract.options.jsonInterface, auctionAddress);
                        
                        // Call placeBid() on the specific Auction contract and get receipt
                        //const receipt = await specificAuctionContract.methods.placeBid().send({ from: account, value: web3.utils.toWei(bidAmount, "ether") });
                        const receipt = await auctionManagerContract.methods.placeBidOnAuction(auctionAddress, web3.utils.toWei(bidAmount, "ether")).send({ from: account, value: web3.utils.toWei(bidAmount, "ether") });
                        const bidCreatedEvent = receipt.events.BidPlaced;
                        // Get the transaction hash
                        const transactionHash = receipt.transactionHash;
                        console.log("Transaction Hash:", transactionHash);
                        
                        // Get the block timestamp using the block number
                        const block = await web3.eth.getBlock(receipt.blockNumber);
                        const blockTimestamp = block.timestamp;
                        console.log("Block Timestamp:", new Date(blockTimestamp * 1000));

                        //check tx receipt
                        console.log("Transaction Receipt:", receipt);
                        console.log("bidCreatedEvent:", bidCreatedEvent);
                        console.log("auctionAddress: ", auctionAddress)
                        console.log('Bid placed successfully!');
                        if (bidCreatedEvent) {
                            console.log("Bid Placed on Auction Contract Address: ", auctionAddress);
                            //alert('Bid Placed on Auction Contract Address: ' + auctionAddress);

                            // Assuming your epoch timestamp is in milliseconds
                            //let epoch = blockTimestamp; // Replace with your epoch time

                            // Create a new Date object using the epoch timestamp
                            //let date = new Date(epoch);

                            // Format the date and time to SQL datetime format
                            //let timePlaced = date.toISOString().slice(0, 19).replace('T', ' ');

                            //let timePlaced =  new Date(blockTimestamp * 1000);
                            //console.log(timePlaced); // Output: "YYYY-MM-DD HH:MM:SS"

                            var date = new Date(blockTimestamp * 1000);
                            // Extract the date parts
                            var year = date.getFullYear();
                            var month = ('0' + (date.getMonth() + 1)).slice(-2); // JavaScript months are 0-indexed
                            var day = ('0' + date.getDate()).slice(-2);
                            var hours = ('0' + date.getHours()).slice(-2);
                            var minutes = ('0' + date.getMinutes()).slice(-2);
                            var seconds = ('0' + date.getSeconds()).slice(-2);

                            // Construct the SQL datetime format
                            var timePlaced = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
                            console.log(timePlaced);

                            try {
                                // Data to send to PHP
                                let formData = new FormData();
                                formData.append('auctionID', document.getElementById('auctionID').value);
                                formData.append('amount', document.getElementById('bid').value);
                                formData.append('timePlaced', timePlaced);
                                formData.append('transactionHash', transactionHash);
                                formData.append('auction_contract_address', auctionAddress);
                                
                                // Send data to PHP script

                                const response = await $.ajax({
                                    url: 'addBidDatabase.php',
                                    type: 'POST',
                                    data: formData, 
                                    contentType: false, // Do not set content type header
                                    processData: false, // Do not process data
                                    success: function(response) {
                                        console.log("Success:", response);
                                        alert('Bid placed successfully.');
                                        location.reload();
                                        //location.replace(location.href);
                                    },
                                    error: function(xhr, status, error) {
                                        console.error("Error:", error);
                                        alert('Bid placed failed. ' + error );
                                        location.reload();
                                    }
                                });
                            } catch (error) {
                                console.error('Error sending data to PHP:', error);
                            }
                        } else {
                            alert('Bid created, but transaction not found in events.');
                        }
                    } catch (error) {
                        console.error(error);
                        alert('Error placing bid: ' + error.message);
                    }
                }
                else 
                {
                    alert('You have already placed a bid on this auction.');
                }
            } 
            else
            {
                alert('Bid fail. Owner cannot bid on own auction.');
            }           
        }
        
        async function mineCurrentTimestampBlock() {
            try {
                await web3.currentProvider.sendAsync({
                    method: "evm_mine",
                    params: [],
                    jsonrpc: "2.0",
                    id: new Date().getTime()
                });
                console.log("Block mined");
            } catch (err) {
                console.error("Error mining block: ", err);
            }
        }
    </script>

    </div>

	<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
        	<div class="pswp__bg"></div>
            <div class="pswp__scroll-wrap"><div class="pswp__container"><div class="pswp__item"></div><div class="pswp__item"></div><div class="pswp__item"></div></div><div class="pswp__ui pswp__ui--hidden"><div class="pswp__top-bar"><div class="pswp__counter"></div><button class="pswp__button pswp__button--close" title="Close (Esc)"></button><button class="pswp__button pswp__button--share" title="Share"></button><button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button><button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button><div class="pswp__preloader"><div class="pswp__preloader__icn"><div class="pswp__preloader__cut"><div class="pswp__preloader__donut"></div></div></div></div></div><div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap"><div class="pswp__share-tooltip"></div></div><button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button><button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button><div class="pswp__caption"><div class="pswp__caption__center"></div></div></div></div></div>

</body>

</html>

<!--Footer-->
<?php
    include_once 'footer.php';
?>
<!--End Footer-->
