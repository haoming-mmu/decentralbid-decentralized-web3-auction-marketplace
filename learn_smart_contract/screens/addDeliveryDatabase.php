<?php 
    include ('dataconnection.php');
    session_start();
    	
    echo "<script>console.log('Hi from addDeliveryDatabase.php');</script>";		 
    $auction_id = $_POST["auctionID"]; 
    $address = $_POST["address"]; 
    $auction_contract_address = $_POST["auction_contract_address"]; 
    
    echo "<script>console.log('Auction Objects: $auction_id + $address + $auction_contract_address');</script>";		
    
    $Index = 1;
    $delivery_id = sprintf("D%04d", $Index);
    $idCheckSQL = "SELECT deliveryID from Delivery ORDER BY deliveryID";
    $idQuery = mysqli_query($connect, $idCheckSQL); 

    while ($idResult = mysqli_fetch_assoc($idQuery)) {
        if($idResult['deliveryID'] == $delivery_id)
        {
            $Index += 1;
            $delivery_id = sprintf("D%04d", $Index);
        }
    }

    // use sql to query auction table to get winning_bid_bidid, seller and buyer userID
    $sql = "SELECT a.highest_bid_bidid, a.SellerUserID, b.BuyerUserID, b.amount 
        FROM auction a 
        INNER JOIN bid b ON a.highest_bid_bidid = b.BidID 
        WHERE a.auction_contract_address = '".$auction_contract_address."' 
        AND a.AuctionID = '".$auction_id."'";
    $result = mysqli_query($connect, $sql);
    $highest_bid_bidid = null;
    $sellerUserID = null;
    $buyerUserID = null;
    $amount = null;
    while ($row1 = mysqli_fetch_assoc($result))
    {
        $highest_bid_bidid = $row1['highest_bid_bidid'];
        $sellerUserID = $row1['SellerUserID'];
        $buyerUserID = $row1['BuyerUserID'];
        $amount = $row1['amount'];
    }

    // insert into delivery
    $query = "INSERT INTO delivery (deliveryID, address, status, auctionID, sellerUserID, buyerUserID) values ('$delivery_id', '$address', 'receiving', '$auction_id', '$sellerUserID', '$buyerUserID')";																																																													
    if(mysqli_query($connect, $query)) {
        if(mysqli_affected_rows($connect) >0)
        {
            $Index = 1;
            $escrow_id = sprintf("E%04d", $Index);
            $idCheckSQL = "SELECT escrowID from Escrow ORDER BY escrowID";
            $idQuery = mysqli_query($connect, $idCheckSQL); 
        
            while ($idResult = mysqli_fetch_assoc($idQuery)) {
                if($idResult['escrowID'] == $escrow_id)
                {
                    $Index += 1;
                    $escrow_id = sprintf("E%04d", $Index);
                }
            }
            
            // insert into escrow
            $queryEscrow = "INSERT INTO escrow (escrowID, status, amount, deliveryID, auctionID, sellerUserID, buyerUserID) values ('$escrow_id', 'receiving', '$amount', '$delivery_id', '$auction_id', '$sellerUserID', '$buyerUserID')";																																																													
            mysqli_query($connect, $queryEscrow);
            if(mysqli_affected_rows($connect) >0) {
                $reply = 0;
                $status = "success";
                $response = " New escrow added successfully. (added to delivery table).";
            }
            else
            {
                $reply = 1;
                $status = "failed"; 
                $response = "Add new escrow Unsuccessful.";   
            }
        }
        else
        {
            $reply = 1;
            $status = "failed"; 
            $response = "Add new delivery Unsuccessful.";   
        }
    } else {
        ?>
        <script type="text/javascript">
            console.log('Error: <?= mysqli_error($connect); ?>');
        </script>
        <?php
    }
?>