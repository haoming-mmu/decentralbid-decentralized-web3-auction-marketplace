<?php 
    include ('dataconnection.php');
    session_start();
    	
    echo "<script>console.log('Hi from addDisputeDatabase.php');</script>";		 
    $auction_id = $_POST["auctionID"]; 
    $auction_contract_address = $_POST["auction_contract_address"]; 
    $description = $_POST["description"];
    $escrow_id = null;
    
    echo "<script>console.log('Auction Objects: $auction_id + $auction_contract_address + $description');</script>";	
    
    $Index = 1;
    $dispute_id = sprintf("DS%04d", $Index);
    $idCheckSQL = "SELECT disputeID from dispute ORDER BY disputeID";
    $idQuery = mysqli_query($connect, $idCheckSQL); 

    while ($idResult = mysqli_fetch_assoc($idQuery)) {
        if($idResult['disputeID'] == $dispute_id)
        {
            $Index += 1;
            $dispute_id = sprintf("DS%04d", $Index);
        }
    }

    $escrowIDSQL = "SELECT escrowID from escrow where auctionID = '$auction_id'";
    $escrowIDQuery = mysqli_query($connect, $escrowIDSQL); 
    while ($escrowIDResult = mysqli_fetch_assoc($escrowIDQuery)) {
        $escrow_id = $escrowIDResult['escrowID'];
    }

    $query = "INSERT INTO dispute (DisputeID, Description, status, AuctionID, RaisedByUserID, EscrowID) values ('$dispute_id', '$description', 'active', '$auction_id', '{$_SESSION['userID']}', '$escrow_id')";																																																													
    if(mysqli_query($connect, $query)) {
        if(mysqli_affected_rows($connect) >0)
        {
            $reply = 0;
            $status = "success"; 
            $response = "Update dispute successful."; 
        }
        else 
        {
            $reply = 1;
            $status = "failed"; 
            $response = "Update dispute unsuccessful."; 
            ?>
            <script type="text/javascript">
                console.log('Error: <?= mysqli_error($connect); ?>');
            </script>
            <?php  
        }
    }

?>