<?php 
    include ('dataconnection.php');
    session_start();
    	
    echo "<script>console.log('Hi from addAuctionDatabase.php');</script>";		 
    $product_name = $_POST["productname"]; 
    $product_category = $_POST["productcategory"]; 	
    $auction_start_datetime = $_POST["auction_start_datetime"];
    $auction_duration = $_POST["auction_duration"];
    $auction_end_datetime = $_POST["auction_end_datetime"];
    $condition = $_POST["condition"];
    $description = $_POST["description"];
    $reserved_price = $_POST["reserved_price"];
    $auction_contract_address = $_POST["auction_contract_address"];
    $auctionStatus = "active"; 
    //$frontImgName = $_POST["frontImgName"];
    //$backImgName = $_POST["backImgName"];
    

    echo "<script>console.log('Auction Objects: $product_name + $product_category + $auction_start_datetime + $auction_duration + $auction_end_datetime + $condition + $description + $reserved_price + $auction_contract_address');</script>";		
    $frontImgName = $_FILES["productfrontimg"]["name"];
    // For front image upload
    $target_dir = "assets/images/productimg/";
    $target_file = $target_dir . basename($frontImgName);
    
    $backImgName = $_FILES["productbackimg"]["name"];
    // For image upload
    $target_dir2 = "assets/images/productimg/";
    $target_file2 = $target_dir2 . basename($backImgName);
    
    $Index = 1;
    $auction_id = sprintf("A%04d", $Index);
    $idCheckSQL = "SELECT auctionID from auction ORDER BY auctionID";
    $idQuery = mysqli_query($connect, $idCheckSQL); 

    while ($idResult = mysqli_fetch_assoc($idQuery)) {
        if($idResult['auctionID'] == $auction_id)
        {
            $Index += 1;
            $auction_id = sprintf("A%04d", $Index);
        }
    }

    $query = "INSERT INTO auction (auctionID, ItemName, StartTime, duration, EndTime, product_condition, description, status, reserved_price, category_id, SellerUserID, product_front_image, product_back_image, auction_contract_address) values ('$auction_id', '$product_name', '$auction_start_datetime', '$auction_duration', '$auction_end_datetime', '$condition', '$description', '$auctionStatus', '$reserved_price', '$product_category', '{$_SESSION['userID']}', '$frontImgName', '$backImgName', '$auction_contract_address')";																																																													
    if(mysqli_query($connect, $query)) {
        ?>
        <script>
            Swal.fire('<?= $auction_id ?>', 'added successfully', 'success');
        </script>
        <?php
        // Image upload inside if block
        $uploadBackImg = move_uploaded_file($_FILES["productbackimg"]["tmp_name"], $target_file2);
        $uploadFrontImg = move_uploaded_file($_FILES["productfrontimg"]["tmp_name"], $target_file);

        if ($uploadBackImg) {
            echo '<script>alert("Product back image uploaded.");</script>';
        }
        if ($uploadFrontImg) {
            echo '<script>alert("Product front image uploaded.");</script>';
        }
        if(mysqli_affected_rows($connect) >0)
        {
            $reply = 0;
            $status = "success";
            $response = " Add New Auction Successfully. (added to Auction table).";
        }
        else
        {
            $reply = 1;
            $status = "failed"; 
            $response = "Add New Auction Unsuccessful.";   
        }
    } else {
        ?>
        <script type="text/javascript">
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'Product add failed.' });
            console.log('Error: <?= mysqli_error($connect); ?>');
        </script>
        <?php
    }
?>