<?php
    include_once 'header.php';
?>
<?php include("dataconnection.php"); 
    // Check connection
    if (!$connect) {
      die("Connection failed: " . mysqli_connect_error());
    }
    date_default_timezone_set("Asia/Kuala_Lumpur");
    $date = date('Y-m-d');
?>
    <!--End Header-->

<!--        JAVASCRIPT        -->
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
    function checking()
    {
        var fname = $("#firstname").val();
        var lname = $("#lastname").val();
        var email = $("#email").val();
        var phone = $("#telephone").val();
        var add1 = $("#address_1").val();
        var add2 = $("#address_2").val();
        var state = $("#state").val();
        var city = $("#city").val();
        var postcode = $("#postcode").val();
        
        if(fname.length>0 && lname.length>0 && email.length>0 && phone.length>0 && add1.length>0 && add2.length>0 && state.length>0 && city.length>0 && postcode.length>0)
        {
            ccardPayment();
        }
        else
        {
            alert("Please fill in your billing details.");
        }
    }
    function checkE()
    {
        var fname = $("#firstname").val();
        var lname = $("#lastname").val();
        var email = $("#email").val();
        var phone = $("#telephone").val();
        var add1 = $("#address_1").val();
        var add2 = $("#address_2").val();
        var state = $("#state").val();
        var city = $("#city").val();
        var postcode = $("#postcode").val();
        
        if(fname.length>0 && lname.length>0 && email.length>0 && phone.length>0 && add1.length>0 && add2.length>0 && state.length>0 && city.length>0 && postcode.length>0)
        {
            ewalletPayment();
        }
        else
        {
            alert("Please fill in your billing details.");
        }
    }
    function ewalletPayment()
    {
        console.log("Call ewalletPayment.php file");
        
        var walletID = $("#walletID").val();
        var pin = $("#custEwalletPin").val();
        var ewalletPin = $("#ewalletPin").val();
        var t = $("#cartTable #cartTotal").html();
        var tt = t.replace(/[^\d.-]/g, '');
        var total = tt;
        var fname = $("#firstname").val();
        var lname = $("#lastname").val();
        var email = $("#email").val();
        var phone = $("#telephone").val();
        var add1 = $("#address_1").val();
        var add2 = $("#address_2").val();
        var state = $("#state").val();
        var city = $("#city").val();
        var postcode = $("#postcode").val();

        console.log(total);
        console.log(fname);
        console.log(lname);
        console.log(email);
        console.log(phone);
        console.log(add1);
        console.log(add2);
        console.log(state);
        console.log(postcode);
        console.log(city);
        
        if(pin==ewalletPin)
        {  
            console.log("test open file");
            
            $.ajax({
               url: 'ewalletPayment.php',
               method: 'POST',
               dataType: 'json',
               data:{
                   total: total,
                   walletID: walletID,
                   fname: fname,
                    lname: lname,
                    email: email,
                    phone: phone,
                    add1: add1,
                    add2: add2,
                    state: state,
                    postcode: postcode,
                    city: city
               }, success: function(response){
                    if(response['status'] == 'successTP')
                    {
                        console.log("End Ewallet payment and Topup");
                        console.log(response);
                        alert("Ewallet Payment and auto Topup Successful.");
                        window.location.href="viewtransaction.php";
                    }
                   else if(response['status'] == 'success')
                   {
                       console.log("End Ewallet payment");
                       console.log(response);
                       alert("Ewallet Payment Successful.");
                       window.location.href="viewtransaction.php";
                   }
                   else if(response['status'] == 'success2')
                   {
                       console.log("End Ewallet payment no topup");
                       console.log(response);
                       alert("Ewallet Payment Successful. (No auto topup)");
                       window.location.href="viewtransaction.php";
                   }
                   else if(response['status'] == 'failed1')
                   {
                       alert("Something went wrong. Please try again.");
                       console.log(response);
                   }
                   else if(response['status'] == 'failed2')
                   {
                       alert("Parameter is empty. Please try again.");
                       console.log(response);
                   }
                   else
                   {
                       alert("Unexpected Error. Please try again later.");
                   }

               }, error: function(err) {
                   console.log(err);
                   console.log("hi ewallet payment");
               } 
           });  
        }
        else if(!ewalletPin)
        {
            alert("Please enter Ewallet Pin to continue.");
        }
        else if(ewalletPin.toString().length!=6)
        {
            alert("Please follow the required format. (6 digit pin)");
        }
        else
        {
            alert("Wrong Pin entered. Please try again.");
        }
    }

    function ccardPayment()
    {
        console.log("Call ccardPayment.php file");
        
        //var cardName = ("#input-cardname").val();
        var cardNo = $("#input-cardno").val();
        var cvv = $("#input-cardcvv").val();
        var cardMonth = $("#input-cardmonth").val();
        var cardYear = $("#input-cardyear").val();
        var t = $("#cartTable #cartTotal").html();
        var tt = t.replace(/[^\d.-]/g, '');
        var total = tt;
        var fname = $("#firstname").val();
        var lname = $("#lastname").val();
        var email = $("#email").val();
        var phone = $("#telephone").val();
        var add1 = $("#address_1").val();
        var add2 = $("#address_2").val();
        var state = $("#state").val();
        var city = $("#city").val();
        var postcode = $("#postcode").val();
        
        console.log(total);
        console.log(fname);
        console.log(lname);
        console.log(email);
        console.log(phone);
        console.log(add1);
        console.log(add2);
        console.log(state);
        console.log(postcode);
        console.log(city);
        
        if(!!total)
        {  
            console.log("test open file");   
           $.ajax({
               url: 'ccardPayment.php',
               method: 'POST',
               dataType: 'json',
               data:{  
                total: total,
                //cardName: cardName,
                cardNo: cardNo,
                cvv: cvv,
                cardMonth: cardMonth,
                cardYear: cardYear,
                fname: fname,
                lname: lname,
                email: email,
                phone: phone,
                add1: add1,
                add2: add2,
                state: state,
                postcode: postcode,
                city: city
               }, success: function(response){
                    if(response['status'] == 'success')
                    {
                        console.log("End CCard payment");
                        console.log(response);
                        alert("Credit card payment successful.");
                        window.location.href="viewtransaction.php";
                    }
                    else if(response['status'] == 'failed1')
                    {
                        console.log("End CCard payment");
                        console.log(response);
                        alert("Credit card payment unsuccessful.");
                    }
                    else if(response['status'] == 'failed2')
                    {
                        console.log("End CCard payment");
                        console.log(response);
                        alert("Parameter empty.");
                    }
                    else if(response['status'] == 'failed3')
                    {
                        console.log("End CCard payment");
                        console.log(response);
                        alert("Purchase failed. Expiry month is invalid. (01 - 12 only)");
                    }
                    else if(response['status'] == 'failed4')
                    {
                        console.log("End CCard payment");
                        console.log(response);
                        alert("Purchase failed. Credit Card expired.");
                    }
                   else
                   {
                       alert("Unexpected Error. Please try again later.");
                   }

               }, error: function(err) {
                   console.log(err);
                   console.log("hi credit card payment");
               } 
           });  
        }
        else
        {
            alert("Wrong Pin entered. Please try again.");
        }
    }

</script>

    <!--Body Content-->
    <div id="page-content">
    	<!--Page Title-->
    	<div class="page section-header text-center">
			<div class="page-title">
        		<div class="wrapper"><h1 class="page-width">Checkout</h1></div>
      		</div>
		</div>
        <!--End Page Title-->
        
        <div class="container">
        	<div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-3">
                    <div class="customer-box returning-customer">
                    <?php
                        if(isset($_SESSION["userid"])) {
                            echo "<h3><i class='icon anm anm-user-al'></i> Ready to checkout? <a href='checkout.php' class='text-white text-decoration-underline' data-toggle='collapse'></a></h3>";
                        }
                        else{
                           echo "<h3><i class='icon anm anm-user-al'></i> Returning customer? <a href='login.php' id='customer' class='text-white text-decoration-underline' >Click here to login</a></h3>";
                        }
                      ?>
                        <div id="customer-login" class="collapse customer-content">
                            <div class="customer-info">
                                <form>
                                    <div class="row">
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                            <label for="exampleInputEmail1">Email address <span class="required-f">*</span></label>
                                            <input type="email" class="no-margin" id="exampleInputEmail1">
                                        </div>
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                            <label for="exampleInputPassword1">Password <span class="required-f">*</span></label>
                                            <input type="password" id="exampleInputPassword1">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-check width-100 margin-20px-bottom">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input" value=""> Remember me!
                                                </label>
                                                <a href="#" class="float-right">Forgot your password?</a>
                                            </div>
                                            <button type="submit" class="btn btn-primary mt-3">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-3">
                    <div class="customer-box customer-coupon">
                        <div id="have-coupon" class="collapse coupon-checkout-content">
                            <div class="discount-coupon">
                                <div id="coupon" class="coupon-dec tab-pane active">
                                    <p class="margin-10px-bottom">Enter your coupon code if you have one.</p>
                                    <label class="required get" for="coupon-code"><span class="required-f">*</span> Coupon</label>
                                    <input id="coupon-code" required="" type="text" class="mb-3">
                                    <button class="coupon-btn btn" type="submit">Apply Coupon</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row billing-fields">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 sm-margin-30px-bottom">
                    <div class="create-ac-content bg-light-gray padding-20px-all">
                        <?php 
                            $result = mysqli_query($connect, "SELECT * from customer where cust_ID = '".$_SESSION["userid"]."'");
                            $row = mysqli_fetch_assoc($result);
                        ?>
                        <form name="fillinform">
                            <fieldset>
                                <h2 class="login-title mb-3">Billing details</h2>
                                <div class="row">
                                    <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                        <label for="input-firstname">First Name <span class="required-f">*</span></label>
                                        <input name="firstname" id="firstname" value="<?php if (isset($_POST['firstname'])) echo $_POST['firstname']; else echo htmlspecialchars($row["cust_first_name"]); ?>" type="text" required>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                        <label for="input-lastname">Last Name <span class="required-f">*</span></label>
                                        <input name="lastname" id="lastname" value="<?php if (isset($_POST['lastname'])) echo $_POST['lastname']; else echo htmlspecialchars($row["cust_last_name"]); ?>" type="text" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                        <label for="input-email">E-Mail <span class="required-f">*</span></label>
                                        <input name="email" id="email" value="<?php if (isset($_POST['email'])) echo $_POST['email']; else echo htmlspecialchars($row["cust_email"]); ?>" type="email" required>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                        <label for="input-telephone">Telephone <span class="required-f">*</span></label>
                                        <input name="telephone" id="telephone" value="<?php if (isset($_POST['telephone'])) echo $_POST['telephone']; else echo htmlspecialchars($row["cust_phone_number"]); ?>"  type="tel" required>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="row">
                                    <div class="form-group col-md-6 col-lg-6 col-xl-6">
                                        <label for="input-company">Address 1</label>
                                        <input name="address_1" id="address_1" value="<?php if (isset($_POST['address_1'])) echo $_POST['address_1']; else echo htmlspecialchars($row["cust_address1"]); ?>" type="text" required>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                        <label for="input-address-1">City <span class="required-f">*</span></label>
                                        <input name="city" id="city" value="<?php if (isset($_POST['city'])) echo $_POST['city']; else echo htmlspecialchars($row["cust_city"]); ?>" type="text" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6 col-lg-6 col-xl-6">
                                        <label for="input-address-2">Address 2 <span class="required-f">*</span></label>
                                        <input name="address_2" id="address_2" value="<?php if (isset($_POST['address_2'])) echo $_POST['address_2']; else echo htmlspecialchars($row["cust_address2"]); ?>" type="text" required>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                        <label for="input-city">State<span class="required-f">*</span></label>
                                        <input name="state" id="state" value="<?php if (isset($_POST['state'])) echo $_POST['state']; else echo htmlspecialchars($row["cust_state"]); ?>" type="text" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                        <label for="input-postcode">Post Code <span class="required-f">*</span></label>
                                        <input name="postcode" id="postcode" value="<?php if (isset($_POST['postcode'])) echo $_POST['postcode']; else echo htmlspecialchars($row["cust_postcode"]); ?>" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' minlength="5" maxlength="5" size="5" required>
                                    </div>
                                </div> 
                            </fieldset>
<!--                             <button class="btn" name="fillbtn" value="Billing Details" type="submit" >Submit</button>
 -->                        </form>
                    </div>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                    <div class="your-order-payment">
                        <div class="your-order">
                            <h2 class="order-title mb-4">Your Order</h2>

                            <div class="table-responsive-sm order-table"> 
                                <table id="cartTable" class="bg-white table table-bordered table-hover text-center">
                                    <thead>
                                        <tr>
                                            <th class="text-left">Product Name</th>
                                            <th>Price</th>
                                            <th>condition</th>
                                            <th>Description</th>
                                            <th>Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                            $result = mysqli_query($connect, "SELECT cart_item.cart_item_id, cart_item.fk_product_id, cart_item.quantity, cart_item.price, cart_item.total_cost, cart_item.size from cart_item
                                            JOIN cart on cart.cart_id = cart_item.fk_cart_id 
                                            JOIN customer ON customer.cust_ID = cart.user_id_cart 
                                            WHERE cart.user_id_cart ='".$_SESSION["userid"]."' 
                                            ORDER BY cart_item_id DESC");
                                            $subtotal=0.00;
                                            while($row = mysqli_fetch_assoc($result)) 
                                            { 
                                                $subtotal+=$row["total_cost"];
                                                $prodResult = mysqli_query($connect, "SELECT product.product_id, product.product_front_image, product.product_name, product.fk_colour_id from product where product.product_id = '".$row["fk_product_id"]."'");
                                                $prodRow = mysqli_fetch_assoc($prodResult);
                                        ?>
                                        <tr>
                                            <td class="text-left"><a href="<?php printf('%s?product_id=%s', 'product-layout.php',  $prodRow['product_id']); ?>"><?php echo $prodRow["product_name"]; ?> </a></td>
                                            <td>RM <?php echo $row["price"]; ?></td>
                                            <td><?php echo $prodRow["fk_colour_id"]; ?></td>
                                            <td style="text-transform:uppercase"><?php echo $row["size"]; ?></td>
                                            <td>RM <?php echo $row["total_cost"]; ?></td>
                                        </tr>
                                        <?php
                                            }
                                        ?>
                                    </tbody>
                                    <tfoot class="font-weight-600">
                                        <tr>
                                            <td colspan="4" class="text-right">Shipping </td>
                                            <td>RM 00.00</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="text-right">Total</td>
                                            <td id="cartTotal">RM <?php echo $subtotal; ?></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <hr />
                                <div class="order-button-payment">
                                    <button class="btn" value="Place order" type="submit" onClick="checking();" >Place order</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Body Content-->
    
    <!--Footer-->
    <?php
    include_once 'footer.php';
?>
    <!--End Footer-->
    <!--Scoll Top-->
    <span id="site-scroll"><i class="icon anm anm-angle-up-r"></i></span>
    <!--End Scoll Top-->
    
     <!-- Including Jquery -->
     <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
     <script src="assets/js/vendor/jquery.cookie.js"></script>
     <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
     <script src="assets/js/vendor/wow.min.js"></script>
     <!-- Including Javascript -->
     <script src="assets/js/bootstrap.min.js"></script>
     <script src="assets/js/plugins.js"></script>
     <script src="assets/js/popper.min.js"></script>
     <script src="assets/js/lazysizes.js"></script>
     <script src="assets/js/main.js"></script>
</div>
</body>

<!-- belle/checkout.html   11 Nov 2019 12:44:33 GMT -->
</html>