<?php 
    include ('dataconnection.php');
    session_start();
    	
    echo "<script>console.log('Hi from rejectDisputeDatabase.php');</script>";		 
    $disputeID = $_POST["disputeID"]; 
    $auction_contract_address = $_POST["auction_contract_address"]; 
    $transactionHash = $_POST["transactionHash"];
    
    echo "<script>console.log('Dispute Objects: $disputeID + $auction_contract_address + $transactionHash ');</script>";	
    
    $auctionIDSQL = "SELECT auctionID from dispute where disputeID = '$disputeID'";
    $auctionIDQuery = mysqli_query($connect, $auctionIDSQL); 
    while ($auctionIDResult = mysqli_fetch_assoc($auctionIDQuery)) {
        $auctionID = $auctionIDResult['auctionID'];
    }

    $query = "UPDATE dispute SET status = 'refunded' WHERE disputeID = '$disputeID'";																																																													
    if(mysqli_query($connect, $query)) {
        if(mysqli_affected_rows($connect) >0)
        {
            $updateAuctionQuery = "UPDATE auction SET status='completed' where auctionID = '$auctionID'";
            mysqli_query($connect, $updateAuctionQuery);
            if(mysqli_affected_rows($connect) >0)
            {
                $updateEscrowQuery = "UPDATE escrow SET status='completed', transactionHash='$transactionHash' where auctionID = '$auctionID'";
                mysqli_query($connect, $updateEscrowQuery);
                if (mysqli_affected_rows($connect) >0)
                {
                    $reply = 3;
                    $status = "success"; 
                    $response = "Update dispute and auction and escrow successful.";    
                }
                else
                {
                    $reply = 4;
                    $status = "failed"; 
                    $response = "Update escrow unsuccessful."; 
                }
            }
            else
            {
                $reply = 1;
                $status = "failed"; 
                $response = "Update dispute and auction unsuccessful."; 
                ?>
                <script type="text/javascript">
                    console.log('Error: <?= mysqli_error($connect); ?>');
                </script>
                <?php  
            }
        }
        else 
        {
            $reply = 2;
            $status = "failed"; 
            $response = "Update dispute unsuccessful."; 
            ?>
            <script type="text/javascript">
                console.log('Error: <?= mysqli_error($connect); ?>');
            </script>
            <?php  
        }
    }

?>