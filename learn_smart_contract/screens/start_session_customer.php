<?php
// start_session.php
session_start();
if (isset($_POST['account'])) {
    $_SESSION['user_wallet_address'] = $_POST['account'];
    // Any other session variables or login logic
    echo "Session started for account: " . $_POST['account'];
} else {
    echo "No account provided!";
}
?>