<?php
    include_once 'header.php';
    include ('dataconnection.php');
?>

<!-- Bootstrap CSS -->


<!-- Bootstrap JS and its dependencies -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<!--Body Content-->
<div id="page-content">
    	<!--Page Title-->
    	<div class="page section-header text-center">
			<div class="page-title">
        		<div class="wrapper"><h1 class="page-width">My Bids</h1></div>
      		</div>
		</div>
        <!--End Page Title-->
        
        <div class="container">
        	<div class="row">
            <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 mb-3">
                    <div class="customer-box returning-customer">
                    <?php
                        if(isset($_SESSION["userID"])) 
                        {
                            //fetch all auctions related to active auctions (haven't start and ongoing)
                            $sql = "SELECT * from auction WHERE winner_userid = '".$_SESSION["userID"]."' AND status = 'settlement' AND (CONVERT_TZ(NOW(), @@session.time_zone, '+08:00') > EndTime)";
                            $result = mysqli_query($connect, $sql);                            
                            echo "<h3><i class='icon anm anm-user-al'></i> Successful Bids <a class='text-white text-decoration-underline' data-toggle='collapse'></a></h3>";
                            while ($row = mysqli_fetch_assoc($result))
                            {
                            ?>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div class="your-order-payment">
                                    <div class="your-order">
                                        <h2 class="order-title mb-4">Auction <?php echo $row['auctionID']; ?></h2>

                                        <div class="table-responsive-sm order-table"> 
                                            <table id="cartTable" class="bg-white table table-bordered table-hover text-center">
                                                <thead>
                                                    <tr>
                                                        <th class="text-left">Product Name</th>
                                                        <th>Front Image</th>
                                                        <th>Back Image</th>
                                                        <th>Condition</th>
                                                        <th>Description</th>
                                                        <th>Auction Start DateTime</th>
                                                        <th>Auction End DateTime</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $sqll = "SELECT auctionID, ItemName, product_front_image, product_back_image, product_condition, description, StartTime, EndTime, reserved_price, highest_bid_bidid, auction_contract_address FROM auction WHERE AuctionID = '".$row['AuctionID']."'";
                                                        $resultt = mysqli_query($connect, $sqll);
                                                        
                                                        while ($row2 = mysqli_fetch_assoc($resultt))
                                                        {

                                                            $prodResult = mysqli_query($connect, "SELECT auctionID, ItemName, product_front_image, product_back_image, product_condition, description, StartTime, EndTime, reserved_price, highest_bid_bidid, auction_contract_address FROM auction WHERE AuctionID = '".$row['AuctionID']."'");
                                                            $prodRow = mysqli_fetch_assoc($prodResult);
                                                    ?>
                                                    <tr>
                                                        <td class="text-left"><a href="<?php printf('%s?auctionID=%s', 'product-layout.php',  $prodRow['auctionID']); ?>"><?php echo $prodRow['ItemName'];  ?></a><input name="auction_contract_address" class="auction_contract_address" type="hidden" data-auction-id="<?php echo $prodRow['auctionID']; ?>" value="<?php echo $prodRow['auction_contract_address']; ?>"></td>
                                                        <td><img src="./assets/images/productimg/<?php echo $prodRow['product_front_image']; ?>" width="100" height="100"></td>
                                                        <td><img src="./assets/images/productimg/<?php echo $prodRow['product_back_image']; ?>" width="100" height="100"></td>
                                                        <td><?php echo $row2['product_condition']; ?></td>
                                                        <td><?php echo $row2['description']; ?></td>
                                                        <td><?php echo $prodRow['StartTime']; ?></td>
                                                        <td><?php echo $prodRow['EndTime']; ?></td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                                <tfoot class="font-weight-600">
                                                    <tr>
                                                        <!--
                                                        <td colspan="4" class="text-right">Shipping </td>
                                                        <td>RM 50.00</td>
                                                    </tr>
                                                        -->
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>          
                                </div>
                                <div class="order-button-payment">
                                    <?php 
                                        $deliveryExistResult = mysqli_query($connect, "SELECT * from delivery WHERE AuctionID = '".$prodRow['auctionID']."'");
                                        $deliveryExist = mysqli_fetch_assoc($deliveryExistResult);
                                        if(mysqli_num_rows($deliveryExistResult) > 0)
                                        {
                                            ?>
                                                <button class="btn btn-view-dispute" value="View Delivery Address" type="button" onclick="viewDeliveryAddressModal('<?php echo $prodRow['auctionID']; ?>')" style="background-color: grey; color: white; padding: 10px 20px; border: none; text-transform: uppercase; font-weight: bold; border-radius: 4px; cursor: pointer; " >View Delivery Address</button>
                                            <?php
                                        }
                                        else
                                        {
                                            ?>
                                                <button class="btn btn-enter-delivery" value="Enter Delivery Address" type="button" onclick="enterDeliveryAddressModal('<?php echo $prodRow['auctionID']; ?>')" style="background-color: grey; color: white; padding: 10px 20px; border: none; text-transform: uppercase; font-weight: bold; border-radius: 4px; cursor: pointer; " >Enter Delivery Address</button>
                                            <?php
                                        }
                                    ?>
                                </div>
                                <!-- Delivery Address Modal -->
                                <div class="modal fade" id="deliveryAddressModal-<?php echo $prodRow['auctionID']; ?>" tabindex="-1" role="dialog" aria-labelledby="deliveryAddressModalLabel-<?php echo $prodRow['auctionID']; ?>" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="deliveryAddressModalLabel">Enter Delivery Address</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <!-- Form for delivery address -->
                                                <form id="deliveryAddressForm">
                                                    <div class="form-group">
                                                        <label for="address">Address</label>
                                                        <textarea class="form-control" id="address" rows="3"></textarea>
                                                    </div>
                                                    <!-- Add more fields as needed -->
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary" onclick=" submitDeliveryAddress('<?php echo $prodRow['auctionID']; ?>')">Save Address</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- View Delivery Modal -->
                                <div class="modal fade" id="viewDeliveryModal-<?php echo $prodRow['auctionID']; ?>" tabindex="-1" role="dialog" aria-labelledby="disputeModalLabel-<?php echo $prodRow['auctionID']; ?>" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="deliveryModalLabel">Enter Delivery Address</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <?php 
                                                    $sqlDelivery = "SELECT * from delivery where deliveryID = '".$deliveryExist['deliveryID']."'";
                                                    $resultDelivery = mysqli_query($connect, $sqlDelivery);
                                                    
                                                    while ($rowDelivery = mysqli_fetch_assoc($resultDelivery))
                                                    {
                                                ?>
                                                <!-- Form for delivery address -->
                                                <form id="deliveryAddressForm">
                                                    <div class="form-group">
                                                        <label for="address">Description</label>
                                                        <textarea class="form-control" id="address" rows="3" value="<?php echo $rowDelivery['address']; ?>" readonly ><?php echo $rowDelivery['address']; ?></textarea>
                                                    </div>
                                                    <!-- Add more fields as needed -->
                                                </form>
                                                <?php } ?> 
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <br />
                            <?php }
                            //fetch all unsuccessful bids auctions
                            $sql = "SELECT a.auctionID, a.ItemName, a.product_front_image, a.product_back_image, a.product_condition, a.description, a.StartTime, a.EndTime, a.reserved_price, 
                            a.highest_bid_bidid, a.auction_contract_address, b.BidID, b.Amount, b.BuyerUserID 
                            FROM auction a
                            INNER JOIN bid b ON a.AuctionID = b.AuctionID
                            WHERE b.BuyerUserID != a.winner_userid
                            AND b.BuyerUserID = '".$_SESSION["userID"]."'";
                            $result = mysqli_query($connect, $sql);                            
                            echo "<h3><i class='icon anm anm-user-al'></i> Unsuccessful Bids <a class='text-white text-decoration-underline' data-toggle='collapse'></a></h3>";
                            while ($row = mysqli_fetch_assoc($result))
                            {
                            ?>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div class="your-order-payment">
                                    <div class="your-order">
                                        <h2 class="order-title mb-4">Auction <?php echo $row['auctionID']; ?></h2>

                                        <div class="table-responsive-sm order-table"> 
                                            <table id="cartTable" class="bg-white table table-bordered table-hover text-center">
                                                <thead>
                                                    <tr>
                                                        <th class="text-left">Product Name</th>
                                                        <th>Front Image</th>
                                                        <th>Back Image</th>
                                                        <th>Condition</th>
                                                        <th>Description</th>
                                                        <th>Auction Start DateTime</th>
                                                        <th>Auction End DateTime</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $sqll = "SELECT auctionID, ItemName, product_front_image, product_back_image, product_condition, description, StartTime, EndTime, reserved_price, highest_bid_bidid, auction_contract_address FROM auction WHERE AuctionID = '".$row['auctionID']."'";
                                                        $resultt = mysqli_query($connect, $sqll);
                                                        
                                                        while ($row2 = mysqli_fetch_assoc($resultt))
                                                        {

                                                            $prodResult = mysqli_query($connect, "SELECT auctionID, ItemName, product_front_image, product_back_image, product_condition, description, StartTime, EndTime, reserved_price, highest_bid_bidid, auction_contract_address FROM auction WHERE AuctionID = '".$row['auctionID']."'");
                                                            $prodRow = mysqli_fetch_assoc($prodResult);
                                                    ?>
                                                    <tr>
                                                        <td class="text-left"><a href="<?php printf('%s?auctionID=%s', 'product-layout.php',  $prodRow['auctionID']); ?>"><?php echo $prodRow['ItemName'];  ?></a><input name="auction_contract_address" class="auction_contract_address" type="hidden" data-auction-id="<?php echo $prodRow['auctionID']; ?>" value="<?php echo $prodRow['auction_contract_address']; ?>"></td>
                                                        <td><img src="./assets/images/productimg/<?php echo $prodRow['product_front_image']; ?>" width="100" height="100"></td>
                                                        <td><img src="./assets/images/productimg/<?php echo $prodRow['product_back_image']; ?>" width="100" height="100"></td>
                                                        <td><?php echo $row2['product_condition']; ?></td>
                                                        <td><?php echo $row2['description']; ?></td>
                                                        <td><?php echo $prodRow['StartTime']; ?></td>
                                                        <td><?php echo $prodRow['EndTime']; ?></td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                                <tfoot class="font-weight-600">
                                                    <tr>
                                                        <!--
                                                        <td colspan="4" class="text-right">Shipping </td>
                                                        <td>RM 50.00</td>
                                                    </tr>
                                                        -->
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>          
                                </div>
                                <div class="order-button-payment">
                                    
                                </div>
                                <!-- Delivery Address Modal -->
                                <div class="modal fade" id="deliveryAddressModal-<?php echo $prodRow['auctionID']; ?>" tabindex="-1" role="dialog" aria-labelledby="deliveryAddressModalLabel-<?php echo $prodRow['auctionID']; ?>" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="deliveryAddressModalLabel">Enter Delivery Address</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <!-- Form for delivery address -->
                                                <form id="deliveryAddressForm">
                                                    <div class="form-group">
                                                        <label for="address">Address</label>
                                                        <textarea class="form-control" id="address" rows="3"></textarea>
                                                    </div>
                                                    <!-- Add more fields as needed -->
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary" onclick=" submitDeliveryAddress('<?php echo $prodRow['auctionID']; ?>')">Save Address</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <br />
                            <?php }
                        }
                        else{
                           echo "<h3><i class='icon anm anm-user-al'></i> Please login to view order. <a href='login.php' id='customer' class='text-white text-decoration-underline' >Click here to login</a></h3>";
                        }
                      ?>
                        
                    </div>
                </div>
        </div>   
</div>
    <!--End Body Content-->
    <!-- JavaScript files-->
    <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="js/front.js"></script>
        <!-- Including Jquery -->
        <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
        <script src="assets/js/vendor/jquery.cookie.js"></script>
        <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
        <script src="assets/js/vendor/wow.min.js"></script>
        <!-- Including Javascript -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/lazysizes.js"></script>
        <script src="assets/js/main.js"></script>
        <!-- Javascript functions -->
        <script>

    function viewDeliveryAddressModal(auctionID) {
        // Use the auctionID to target the correct modal
        console.log("open view delivery address modal for: " + auctionID);
        $('#viewDeliveryModal-' + auctionID).modal('show');
    }

    function enterDeliveryAddressModal(auctionID) {
        // Use the auctionID to target the correct modal
        console.log("open delivery modal for: " + auctionID);
        $('#deliveryAddressModal-' + auctionID).modal('show');
    }

    async function submitDeliveryAddress(auctionID) {
        // Get the address value from the correct modal
        var address = document.querySelector('#deliveryAddressModal-' + auctionID + ' #address').value;
        
        console.log("Auction ID: " + auctionID);
        console.log("Address submitted: " + address);
        const auctionAddressInput = document.querySelector('.auction_contract_address[data-auction-id="' + auctionID + '"]');
        const auctionAddress = auctionAddressInput.value;
        console.log('auction Address: ' + auctionAddress);

        // TODO: Perform AJAX request to send the address to the server
        try {
            // Data to send to PHP
            let formData = new FormData();
            formData.append('auctionID', auctionID);
            formData.append('auction_contract_address', auctionAddress);
            formData.append('address', address);
            
            // Send data to PHP script
            const response = await $.ajax({
                url: 'addDeliveryDatabase.php',
                type: 'POST',
                data: formData, 
                contentType: false, // Do not set content type header
                processData: false, // Do not process data
                success: function(response) {
                    console.log("Success:", response);
                    alert('Delivery added successfully.');
                    location.reload();
                },
                error: function(xhr, status, error) {
                    console.error("Error:", error);
                    alert('Add delivery failed. ' + error );
                    location.reload();
                }
            });
        } catch (error) {
            console.error('Error sending data to PHP:', error);
        }

        // Close the correct modal
        $('#deliveryAddressModal-' + auctionID).modal('hide');
    }

    
    </script>
</script>
<?php
    include("footer.php");
?>

