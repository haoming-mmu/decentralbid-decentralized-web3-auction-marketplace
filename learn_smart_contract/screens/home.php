<?php

    include_once 'header.php';
?>
    
    <!--Body Content-->
    <div id="page-content">
    	<!--Home slider-->
    	<div class="slideshow slideshow-wrapper pb-section sliderFull">
        	<div class="home-slideshow">
            	<div class="slide">
                	<div class="blur-up lazyload bg-size">
                        <img class="blur-up lazyload bg-img" data-src="assets/images/slideshow-banners/belle-banner1.jpg" src="assets/images/slideshow-banners/belle-banner1.jpg" alt="Shop Our New Collection" title="Shop Our New Collection" />
                        <div class="slideshow__text-wrap slideshow__overlay classic bottom">
                            <div class="slideshow__text-content bottom">
                                <div class="wrap-caption center">
                                        <h2 class="h1 mega-title slideshow__title">Discover New Items on Auction</h2>
                                        <span class="mega-subtitle slideshow__subtitle">From High to low, pre-loved or new. We have you covered</span>
                                        <span class="btn" onclick = "location.href='viewproduct.php'">Bid now</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Home slider-->

            <!--Featured Product-->
        <div class="product-rows section">
        	<div class="container">
            	<div class="row">
                	<div class="col-12 col-sm-12 col-md-12 col-lg-12">
        				      <div class="section-header text-center">
                            <h2 class="h2">Product Categories</h2>
                      </div>
            		  </div>
              </div>
                <div class="grid-products">
	                <div class="row">
                  <?php 

                   require("dataconnection.php");
                    $sqll = "SELECT product_front_image from auction GROUP BY category_id ORDER BY category_id";
                         $resultt = mysqli_query($connect, $sqll);
                         $currentRow = 1;
                   while ($row = mysqli_fetch_array($resultt)){?>
                        <div class="col-3 col-sm-3 col-md-3 col-lg-3 item grid-view-item style2">
                        	<div class="grid-view_image">
                                <!-- start product image -->
                                <a href="viewProduct.php" class="grid-view-item__link">
                                    <!-- image -->
                                    <img class="" href="viewProduct.php"  data-src="./assets/images/productimg/<?php echo $row["product_front_image"]; ?>" src="./assets/images/productimg/<?php echo $row["product_front_image"]; ?>" alt="<?php echo $row["product_front_image"]; ?>" title="product">
                                    <!-- End image -->
                                </a>
                                <!-- end product image -->
                            </div>
                        </div>
                  <?php $currentRow++; 
                                    } ?>
                  <?php 

                   $sqll = "SELECT category_name, category_id from category ORDER BY category_id";
                         $resultt = mysqli_query($connect, $sqll);
                         $currentRow = 1;
                   while ($row = mysqli_fetch_array($resultt)){?>
                                    <div class="col-3 col-sm-3 col-md-3 col-lg-3 item grid-view-item style2">
                                    <a href="viewproduct.php" class="btn btn--secondary no-border" ><?php echo $row["category_name"] ?></a>
                                    </div>
                                    <!-- End product name -->
                                    
                                    <?php 
                                     $currentRow++; 
                                     if($currentRow % 3 == 0)
                                     {
                                         echo('<br>');
                                     }
                                 } ?>
                	</div>
                </div>
           </div>
        </div>	
        <!--End Featured Product-->
    <!--End Body Content-->
    
     <!--Footer-->
<?php
    include_once 'footer.php';
?>
<!--End Footer-->
    <!--Scoll Top-->
    <span id="site-scroll"><i class="icon anm anm-angle-up-r"></i></span>
    <!--End Scoll Top-->
                            
     <!-- Including Jquery -->
     <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
     <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
     <script src="assets/js/vendor/jquery.cookie.js"></script>
     <script src="assets/js/vendor/wow.min.js"></script>
     <!-- Including Javascript -->
     <script src="assets/js/bootstrap.min.js"></script>
     <script src="assets/js/plugins.js"></script>
     <script src="assets/js/popper.min.js"></script>
     <script src="assets/js/lazysizes.js"></script>
     <script src="assets/js/main.js"></script>
     
    </div>

</body>

</html>