<?php
    include_once 'header.php';
    include ('dataconnection.php');
?>

<!-- Bootstrap CSS -->

<!-- Bootstrap JS and its dependencies -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<!--Body Content-->
<div id="page-content">
    	<!--Page Title-->
    	<div class="page section-header text-center">
			<div class="page-title">
        		<div class="wrapper"><h1 class="page-width">Check User History</h1></div>
      		</div>
		</div>
        <!--End Page Title-->
        
        <div class="container">
        	<div class="row">
            <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 mb-3">
                    <div class="customer-box returning-customer">
                    <?php
                        if(isset($_SESSION["userID"])) 
                        {
                            $userID = "";
                            if(isset($_GET['userID'])) {
                                $userID = $_GET['userID'];
                                echo "<h3><i class='icon anm anm-user-al'></i> User ID passed from previous page: </h3>";

                                // Use the userID to query the database
                                $sql = "SELECT (SELECT COUNT(*) FROM bid WHERE BuyerUserID = '$userID') AS bidCount, (SELECT COUNT(*) FROM auction WHERE SellerUserID = '$userID') AS auctionCount";
                                $result = mysqli_query($connect, $sql);
                                while ($row = mysqli_fetch_assoc($result))
                                {   
                    ?>
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <label for="userID" class="col-form-label">Number of bid placed: <?php echo $row['bidCount']; ?></label>
                            </div>
                            <div class="col-auto">
                                <label for="userID" class="col-form-label">Number of auction created placed: <?php echo $row['auctionCount']; ?></label>
                            </div>
                        </div>
                    <?php       }
                            }     
                            else {
                                echo "<h3><i class='icon anm anm-user-al'></i> No user ID passed from previous page. Please try again. <a href='checkUserHistory.php' id='customer' class='text-white text-decoration-underline' >Click here to try again</a></h3>";
                            }

                            //fetch all bids related to user
                            $sql = "SELECT BidID, AuctionID, TimePlaced FROM bid WHERE BuyerUserID = '".$userID."'"; 
                            $result = mysqli_query($connect, $sql);                            
                            ?>
                            <h2 class="order-title mb-4">Bid History </h2>
                            <?php
                            while ($row = mysqli_fetch_assoc($result))
                            {
                            ?>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div class="your-order-payment">
                                    <div class="your-order">

                                        <div class="table-responsive-sm order-table"> 
                                            <table id="cartTable" class="bg-white table table-bordered table-hover text-center">
                                                <thead>
                                                    <tr>
                                                        <th>Bid ID</th>
                                                        <th>Auction ID</th>
                                                        <th>Date Time Placed</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $sqll = "SELECT BidID, AuctionID, TimePlaced FROM bid WHERE BidID = '".$row['BidID']."' AND BuyerUserID = '".$userID."'"; 
                                                        $resultt = mysqli_query($connect, $sqll);
                                                        
                                                        while ($row2 = mysqli_fetch_assoc($resultt))
                                                        {

                                                            $prodResult = mysqli_query($connect, "SELECT BidID, AuctionID, TimePlaced FROM bid WHERE BidID = '".$row['BidID']."' BuyerUserID = '".$userID."'" );
                                                            $prodRow = mysqli_fetch_assoc($prodResult);
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $row2['BidID']; ?></td>
                                                        <td><a href="<?php printf('%s?auctionID=%s', 'product-layout.php',  $prodRow['AuctionID']); ?>"> <?php echo $row2['AuctionID']; ?> </a></td>
                                                        <td><?php echo $row2['TimePlaced']; ?></td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                                <tfoot class="font-weight-600">
                                                    <tr>
                                                        <!--
                                                        <td colspan="4" class="text-right">Shipping </td>
                                                        <td>RM 50.00</td>
                                                    </tr>
                                                        -->
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>          
                                </div>
                                <div class="order-button-payment">
                                    
                                </div>
                            </div>
                        
                            <br />
                            <?php }

                            //fetch all auction related to user
                            $sql = "SELECT AuctionID, StartTime, EndTime, status FROM auction WHERE SellerUserID = '".$userID."'"; 
                            $result = mysqli_query($connect, $sql);                            
                            ?>
                            <h2 class="order-title mb-4">Auction History </h2>
                            <?php
                            while ($row = mysqli_fetch_assoc($result))
                            {
                            ?>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div class="your-order-payment">
                                    <div class="your-order">

                                        <div class="table-responsive-sm order-table"> 
                                            <table id="cartTable" class="bg-white table table-bordered table-hover text-center">
                                                <thead>
                                                    <tr>
                                                        <th>Auction ID</th>
                                                        <th>Auction Start DateTime</th>
                                                        <th>Auction End DateTime</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $sqll = "SELECT AuctionID, StartTime, EndTime, status FROM auction WHERE AuctionID = '".$row['AuctionID']."' AND SellerUserID = '".$userID."'"; 
                                                        $resultt = mysqli_query($connect, $sqll);
                                                        
                                                        while ($row2 = mysqli_fetch_assoc($resultt))
                                                        {

                                                            $prodResult = mysqli_query($connect, "SELECT AuctionID, StartTime, EndTime, status FROM auction WHERE AuctionID = '".$row['AuctionID']."' AND SellerUserID = '".$userID."'");
                                                            $prodRow = mysqli_fetch_assoc($prodResult);
                                                    ?>
                                                    <tr>
                                                        <td><a href="<?php printf('%s?auctionID=%s', 'product-layout.php',  $prodRow['AuctionID']); ?>"> <?php echo $row2['AuctionID']; ?> </a></td>
                                                        <td><?php echo $row2['StartTime']; ?></td>
                                                        <td><?php echo $row2['EndTime']; ?></td>
                                                        <td><?php echo $row2['status']; ?></td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                                <tfoot class="font-weight-600">
                                                    <tr>
                                                        <!--
                                                        <td colspan="4" class="text-right">Shipping </td>
                                                        <td>RM 50.00</td>
                                                    </tr>
                                                        -->
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>          
                                </div>
                                <div class="order-button-payment">
                                    
                                </div>
                            </div>
                        
                            <br />
                            <?php }
                        }
                        else{
                            echo "<h3><i class='icon anm anm-user-al'></i> Please login to check user's history. <a href='login.php' id='customer' class='text-white text-decoration-underline' >Click here to login</a></h3>";
                        }
                    ?>
                        
                    </div>
                </div>
        </div>   
</div>
    <!--End Body Content-->
    <!-- JavaScript files-->
    <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="js/front.js"></script>
        <!-- Including Jquery -->
        <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
        <script src="assets/js/vendor/jquery.cookie.js"></script>
        <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
        <script src="assets/js/vendor/wow.min.js"></script>
        <!-- Including Javascript -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/lazysizes.js"></script>
        <script src="assets/js/main.js"></script>
        <!-- Javascript functions -->
        <script>
            async function queryUser() {
                var userID = document.getElementById("userIDInput").value;
                console.log("UserID: " + userID);
                if(userID != "") {
                    window.location.href = 'userHistory.php?userID=' + encodeURIComponent(userID);
                }
                else {
                    alert("Please enter a user ID.");
                }
                
            }
        </script>
</script>
<?php
    include("footer.php");
?>

