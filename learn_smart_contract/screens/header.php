<!-- <?php
    session_start();
    include ('dataconnection.php');
    error_reporting(E_ERROR | E_PARSE);
?> -->
<!--- hi --->

<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge">
<script src="https://cdn.jsdelivr.net/npm/web3@1.5.0/dist/web3.min.js"></script>
<title>DecentralBid</title>

 <!-- Bootstrap CDN -->
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<!-- Owl-carousel CDN -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha256-UhQQ4fxEeABh4JrcmAJ1+16id/1dnlOEVCFOxDef9Lw=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha256-kksNxjDRxd/5+jGurZUJd1sdR2v+ClrCl3svESBaJqw=" crossorigin="anonymous" />

<!-- font awesome icons -->
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
<!-- Custom CSS file -->
<link rel="stylesheet" href="style.css">
<meta name="description" content="description">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" href="assets/images/home.png" />
<!-- Plugins CSS -->
<link rel="stylesheet" href="assets/css/plugins.css">
<!-- Bootstap CSS -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<!-- Main Style CSS -->
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/responsive.css">

<style>
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
</style>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

 <?//php

    //require('functions.php');

 ?>    

</head>
<body class="page-template belle">
<div class="pageWrapper">
	<!--Search Form Drawer-->
	<div class="search">
        <div class="search__form">
            <form class="search-bar__form" action="viewproduct.php" method="post">
                <button class="go-btn search__button" name="searchbtn" type="submit"><i class="icon anm anm-search-l"></i></button>
                <input class="search__input" type="search" name="searchinput" value="" placeholder="Search a product..." aria-label="Search" autocomplete="off" required>
            </form>
            <button type="button" class="search-trigger close-btn"><i class="icon anm anm-times-l"></i></button>
        </div>
    </div>
    <!--End Search Form Drawer-->
    <!--Top Header-->
    <div class="top-header">
      <div class="container-fluid">
          <div class="row">
            <div class="col-10 col-sm-8 col-md-5 col-lg-4">
              </div>
              <div class="col-sm-4 col-md-4 col-lg-4 d-none d-lg-none d-md-block d-lg-block">
                <div class="text-center"><p class="top-header_middle-text"> DecentralBid: Decentralized Auction Marketplace </p></div>
              </div>
              <div class="col-2 col-sm-4 col-md-3 col-lg-4 text-right">
                <span class="user-menu d-block d-lg-none"><i class="anm anm-user-al" aria-hidden="true"></i></span>
                  <ul class="customer-links list-inline">

                    <?php
                    if(isset($_SESSION["user_wallet_address"])) {
                        $trimmed_wallet_address = substr($_SESSION["user_wallet_address"], 0, 6);
                        echo "<li>Welcome ".$trimmed_wallet_address."</li>";
                        echo "<li><a href='editProfile.php'>Profile Page</a></li>";
                        echo "<li><a href='logout.php'>Log out</a></li>";
                    }
                    else if(isset($_SESSION["admin_wallet_address"])) {
                        $trimmed_wallet_address = substr($_SESSION["admin_wallet_address"], 0, 6);
                        echo "<li>Welcome admin</li>";
                        echo "<li><a href='updateProfile.php'>Profile Page</a></li>";
                        echo "<li><a href='logout.php'>Log out</a></li>";
                    }
                    else{
                        echo "<li><a href='login.php'>Connect Wallet</a></li>";
                    }
                    ?>
                    <?php
                        $result = mysqli_query($connect, "SELECT cart_item.cart_item_id, cart_item.fk_product_id, cart_item.quantity, cart_item.price, cart_item.total_cost, cart_item.size from cart_item
                        JOIN cart on cart.cart_id = cart_item.fk_cart_id 
                        JOIN customer ON customer.cust_ID = cart.user_id_cart 
                        WHERE cart.user_id_cart ='".$_SESSION["userid"]."' 
                        ORDER BY cart_item_id DESC");
                        if (mysqli_num_rows($result)==0)
                        {
                            $isCartEmpty=1;
                        }
                        else
                        {
                            $isCartEmpty=0;
                        }
                    ?>

                  </ul>
              </div>
          </div>
      </div>
  </div>
  <!--End Top Header-->
    <!--Header-->
    <div class="header-wrap animated d-flex">
    	<div class="container-fluid">        
            <div class="row align-items-center">
            	<!--Desktop Logo-->
                <div class="logo col-md-2 col-lg-2 d-none d-lg-block">
                    <a href="home.php">
                    	<img src="assets/images/logo.png" alt="Stylingo" title="Stylingo" />
                    </a>
                </div>
                <!--End Desktop Logo-->
                <div class="col-2 col-sm-3 col-md-3 col-lg-8">
                	<div class="d-block d-lg-none">
                        <button type="button" class="btn--link site-header__menu js-mobile-nav-toggle mobile-nav--open">
                        	<i class="icon anm anm-times-l"></i>
                            <i class="anm anm-bars-r"></i>
                        </button>
                    </div>
                	<!--Desktop Menu-->
                	<nav class="grid__item" id="AccessibleNav"><!-- for mobile -->
                        <ul id="siteNav" class="site-nav medium center hidearrow">
                            <li class="lvl1 parent megamenu"><a href="viewproduct.php">All Auctions <i class="anm anm-angle-down-l"></i></a>
                            <li class="lvl1 parent megamenu"><a href="viewAuction.php">My Auction <i class="anm anm-angle-down-l"></i></a>
                            <li class="lvl1 parent megamenu"><a href="viewBid.php">My Bid <i class="anm anm-angle-down-l"></i></a>
                            <li class="lvl1 parent megamenu"><a href="viewOrder.php">My Order <i class="anm anm-angle-down-l"></i></a>
                            <li class="lvl1 parent megamenu"><a href="addAuction.php">New Auction <i class="anm anm-angle-down-l"></i></a>
                            <li class="lvl1 parent megamenu"><a href="checkUserHistory.php">Check History <i class="anm anm-angle-down-l"></i></a>
                        </li>
                        <!-- <li><a href="cart.php" class="site-nav" <? //php if ($cartEmpty == 1) {echo "disabled='disabled'";} else{ echo "";} ?>>Cart<i class="anm anm-angle-right-l"></i></a> -->
                        <!-- <li><a href="checkout.php" class="site-nav" <? //php if(isset($_SESSION["userid"])) {if($isCartEmpty==0) {echo "";} else{ echo "style='pointer-events: none'";}} else{ echo "style='pointer-events: none'";} ?>>Checkout</a></li> -->
                    </ul>
                    </nav>
                    <!--End Desktop Menu-->
                </div>
                <div class="col-4 col-sm-3 col-md-3 col-lg-2">
                    <div class="site-header__search">
                    	<button type="button" class="search-trigger" style="color: #000000; line-height: 40px; font-weight: 600;">Search</button>
                    </div>
                </div>
        	</div>
        </div>
    </div>
    <!--End Header-->