      <!--Footer-->
      <footer id="footer">
        <div class="site-footer">
        	<div class="container">
     			<!--Footer Links-->
            	<div class="footer-top">
                	<div class="row">
                    	<div class="col-20 col-sm-20 col-md-4 col-lg-4 footer-links">
                        	<h4 class="h4">Quick Shop</h4>
                            <ul>
                            	<li><a href="#">Electronics</a></li>
                                <li><a href="#">Luxury</a></li>
                                <li><a href="#">Art & Collectibles</a></li>
                                <li><a href="#">Others</a></li>
                            </ul>
                        </div>
                        <div class="col-20 col-sm-20 col-md-4 col-lg-4 footer-links">
                        	<h4 class="h4">Informations</h4>
                            <ul>
                            	<li><a href="viewAuction.php">My Auction</a></li>
                                <li><a href="viewBid.php">My Bid</a></li>
                                <li><a href="viewOrder.php">My Order</a></li>
                                <li><a href="checkUserHistory.php">Check History</a></li>
                            </ul>
                        </div>

                        <div class="col-20 col-sm-20 col-md-4 col-lg-4 contact-box">
                        	<h4 class="h4">Contact Us</h4>
                            <ul class="addressFooter">
                            	<li><i class="fas fa-map-marker-alt" style="colour:white !important;"></i><p>APU, <br>Technology Park Malaysia, KL</p></li>
                               <!-- <i class="fas fa-map-marker-alt"></i>-->
                                <!--<li class="phone"><i class="icon anm anm-phone-s"></i><p>123 456 789</p></li>-->
                                <li class="email"><i class="far fa-envelope" style="colour:white !important;" ></i><p>decentralbid@gmail.com</p></li>
                            </ul>
                        </div>
                    </div>
                </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

<script src="jquery-3.6.0.min.js"></script>    
<script src="sweetalert2.all.min.js"></script>    
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<!-- Font Awesome CSS-->
<link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <!-- Custom Font Icons CSS-->
    
<!-- Owl Carousel Js file -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha256-pTxD+DSzIwmwhOqTFN+DB+nHjO4iAsbgfyFq5K5bcE0=" crossorigin="anonymous"></script>

<!--  isotope plugin cdn  -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js" integrity="sha256-CBrpuqrMhXwcLLUd5tvQ4euBHCdh7wGlDfNz8vbu/iI=" crossorigin="anonymous"></script>

<!-- Custom Javascript -->
    <!--End Footer-->