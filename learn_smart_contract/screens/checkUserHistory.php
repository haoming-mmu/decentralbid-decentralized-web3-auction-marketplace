<?php
    include_once 'header.php';
    include ('dataconnection.php');
?>

<!-- Bootstrap CSS -->

<!-- Bootstrap JS and its dependencies -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<!--Body Content-->
<div id="page-content">
    	<!--Page Title-->
    	<div class="page section-header text-center">
			<div class="page-title">
        		<div class="wrapper"><h1 class="page-width">Check User History</h1></div>
      		</div>
		</div>
        <!--End Page Title-->
        
        <div class="container">
        	<div class="row">
            <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 mb-3">
                    <div class="customer-box returning-customer">
                    <?php
                        if(isset($_SESSION["userID"])) 
                        {
                    ?>
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <label for="userID" class="col-form-label">User ID:</label>
                            </div>
                            <div class="col-auto">
                                <input type="text" name="userID" class="form-control" id="userIDInput">
                            </div>
                        </div>
                        <div class="row mt-3"> 
                            <div class="col">
                                <button class="btn btn-reject-and-close" type="button" onclick="queryUser()">Query</button>
                            </div>
                        </div>
                    <?php        
                        }
                        else{
                           echo "<h3><i class='icon anm anm-user-al'></i> Please login to view order. <a href='login.php' id='customer' class='text-white text-decoration-underline' >Click here to login</a></h3>";
                        }
                      ?>
                        
                    </div>
                </div>
        </div>   
</div>
    <!--End Body Content-->
    <!-- JavaScript files-->
    <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="js/front.js"></script>
        <!-- Including Jquery -->
        <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
        <script src="assets/js/vendor/jquery.cookie.js"></script>
        <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
        <script src="assets/js/vendor/wow.min.js"></script>
        <!-- Including Javascript -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/lazysizes.js"></script>
        <script src="assets/js/main.js"></script>
        <!-- Javascript functions -->
        <script>
            async function queryUser() {
                var userID = document.getElementById("userIDInput").value;
                console.log("UserID: " + userID);
                if(userID != "") {
                    window.location.href = 'userHistory.php?userID=' + encodeURIComponent(userID);
                }
                else {
                    alert("Please enter a user ID.");
                }
                
            }
        </script>
</script>
<?php
    include("footer.php");
?>

