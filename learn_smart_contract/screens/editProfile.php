<?php
    include_once 'header.php';
    include_once 'dataconnection.php';

    if (isset($_POST["submit"])){
        $firstname = mysqli_real_escape_string($connnect, $_POST["firstname"]);
        $lastname= mysqli_real_escape_string($connect, $_POST["lastname"]);
        $email = mysqli_real_escape_string($connect, $_POST["email"]);
        $phone = mysqli_real_escape_string($connect, $_POST["phone"]);
        $address1 = mysqli_real_escape_string($connect, $_POST["address1"]);
        $address2 = mysqli_real_escape_string($connect, $_POST["address2"]);
        $city = mysqli_real_escape_string($connect, $_POST["city"]);
        $state = mysqli_real_escape_string($connect, $_POST["state"]);
        $postcode = mysqli_real_escape_string($connect, $_POST["postcode"]);

        $sql = "UPDATE customer SET (first_name ='$firstname', last_name ='$lastname', Email = '$email', address1 = '$address1', address2 = '$address2', city = '$city', postcode = '$postcode', state = '$state', phone_number = '$phone') WHERE WalletAddress='{$_SESSION['user_wallet_address']}'";
        $result = mysqli_query($connect, $sql);

        if($result){
            echo "<script>alert('Profile is updated successfully.');</script>";
        }
        else{
            echo "<script>alert('Profile can't be updated.');</script>";
            echo $connection ->error;
        }
    }
?>
   
    <!--Body Content-->
    <div id="page-content">
    	<!--Page Title-->
    	<div class="page section-header text-center">
			<div class="page-title">
        		<div class="wrapper"><h1 class="page-width">Update Your Profile</h1></div>
      		</div>
		</div>
        <!--End Page Title-->
        
        <div class="container">
        	<div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 main-col offset-md-3">
                	<div class="mb-4">
                        <input name="userWalletAddress" id="userWalletAddress" type="hidden" value="<?php echo $_SESSION['user_wallet_address'] ; ?>"></input>
                       <form id="customerUpdateForm" accept-charset="UTF-8" class="contact-form" enctype="multipart/form-data">
                           
                       <?php
                        if(isset($_SESSION["userID"])) 
                        {

                        $sql = "SELECT * FROM customer WHERE WalletAddress = '{$_SESSION['user_wallet_address']}'";
                        $result = mysqli_query($connect, $sql);

                        if(mysqli_num_rows($result) > 0){
                            while($row = mysqli_fetch_assoc($result)){
                        ?>  

                        <div class="row">
	                          <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="FirstName">First Name</label>
                                    <input type="text" id="firstname" name="firstname" placeholder="First Name" pattern="[A-Za-z ]{1,32}" value="<?php echo $row['first_name']; ?>" required>
                                </div>
                               </div>
                               <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="LastName">Last Name</label>
                                    <input type="text" id="lastname" name="lastname" placeholder="Last Name" pattern="[A-Za-z ]{1,32}" value="<?php echo $row['last_name']; ?>" required>
                                </div>
                               </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="CustomerEmail">E-mail</label>
                                    <input type="email" id="email" name="email" placeholder="" value="<?php echo $row['Email']; ?>" required>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="CustomerTelephone">Phone Number</label>
                                    <input type="text" id="phoneNumber" pattern="[0-9]{7,14}"  required name="phone" placeholder="Phone Number"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php echo $row['phone_number']; ?>" required>                        	
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="CustomerAddress1">Address 1</label>
                                    <input type="text" id="address1" name="address1" placeholder="" value="<?php echo $row['address1']; ?>" required>                        	
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="CustomerAddress2">Address 2</label>
                                    <input type="text" id="address2" name="address2" placeholder="" value="<?php echo $row['address2']; ?>" required>                        	
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="CustomerCity">City</label>
                                    <input type="text" id="city" name="city" placeholder="" value="<?php echo $row['city']; ?>" required>                        	
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="CustomerState">State</label>
                                    <input type="text" id="state" name="state"  pattern="[A-Za-z ]{1,32}" placeholder="" value="<?php echo $row['state']; ?>" required>                        	
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="CustomerPost">Post code</label>
                                    <input type="text" id="postcode" pattern="[0-9]{5}" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength ="5" name="postcode" placeholder="" value="<?php echo $row['postcode']; ?>" required>                        	
                                </div>
                            </div>

                           
                          </div>
                        
                        
                        <?php
                            }
                        }
                       ?> 
                          
                          <div class="row">
                            <div class="text-center col-12 col-sm-12 col-md-12 col-lg-12">
                            <button type="button" name="submit" class="btn btn-primary1" id="submitBtn" onClick="updateProfile()">Save Changes</button>
                            <p class="mb-4">
									<a href="change-password.php" id="ChangePassword">Change password?</a>
                                </p>
                            </div>
                         </div>
                     </form>
                    </div>
                    <?php
                        }
                        else
                        {
                            echo "<h3><i class='icon anm anm-user-al'></i> Please login to view your profile. <a href='login.php' id='customer' class='text-black text-decoration-underline' >Click here to login</a></h3>";
        
                        }
                    ?>
               	</div>
            </div>
        </div>
       
        
    </div>
    <!--End Body Content-->
   <!--Footer-->
   <?php
    include_once 'footer.php';
        ?>
<!--End Footer-->
    <!--Scoll Top-->
    <span id="site-scroll"><i class="icon anm anm-angle-up-r"></i></span>
    <!--End Scoll Top-->
    
    <!-- Including Jquery -->
    <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
    <script src="assets/js/vendor/jquery.cookie.js"></script>
    <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
    <script src="assets/js/vendor/wow.min.js"></script>
    <!-- Including Javascript -->
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/lazysizes.js"></script>
    <script src="assets/js/main.js"></script>
    <script>
        async function updateProfile() {
                console.log("hi update profile " );
                var userWalletAddress = document.getElementById("userWalletAddress").value;
                var firstname = document.getElementById("firstname").value;
                var lastname = document.getElementById("lastname").value;
                var email = document.getElementById("email").value;
                var phoneNumber = document.getElementById("phoneNumber").value;
                var address1 = document.getElementById("address1").value;
                var address2 = document.getElementById("address2").value;
                var city = document.getElementById("city").value;
                var state = document.getElementById("state").value;
                var postcode = document.getElementById("postcode").value;

                console.log('Values: ' + userWalletAddress + ' ' + firstname + ' ' + lastname + ' ' + email + ' ' + phoneNumber + ' ' + address1 + ' ' + address2 + ' ' + city + ' ' + state + ' ' + postcode);

                if (!firstname || !lastname || !email || !phoneNumber || !address1 || !address2 || !city || !state || !postcode ) {
                    alert('Please fill in the empty field.');
                    console.log('There is empty field.');
                    return;
                }

                // Email Validation using regular expression
                var emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
                if (!emailRegex.test(email)) {
                    alert('Please enter a valid email address.');
                    return;
                }

                try {
                    // Data to send to PHP
                    let formData = new FormData();
                    formData.append('userWalletAddress', userWalletAddress);
                    formData.append('firstname', firstname);
                    formData.append('lastname', lastname);
                    formData.append('email', email);
                    formData.append('phoneNumber', phoneNumber);
                    formData.append('address1', address1);
                    formData.append('address2', address2);
                    formData.append('city', city);
                    formData.append('state', state);
                    formData.append('postcode', postcode);
                    
                    // Send data to PHP script
                    const response = await $.ajax({
                        url: 'updateProfile.php',
                        type: 'POST',
                        data: formData, 
                        contentType: false, // Do not set content type header
                        processData: false, // Do not process data
                        success: function(response) {
                            console.log("Success:", response);
                            alert('Update profile successfully.');
                            location.reload()
                        },
                        error: function(xhr, status, error) {
                            console.error("Error:", error);
                            alert('Update profile failed. ' + error );
                            location.reload()
                        }
                    });
                } catch (error) {
                    console.error('Error sending data to PHP:', error);
                }
            }
    </script>
</div>
</body>
</html>
