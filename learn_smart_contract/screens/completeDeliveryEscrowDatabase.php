<?php 
    include ('dataconnection.php');
    session_start();
    	
    echo "<script>console.log('Hi from addDeliveryDatabase.php');</script>";		 
    $auction_id = $_POST["auctionID"]; 
    $auction_contract_address = $_POST["auction_contract_address"]; 
    $transactionHash = $_POST["transactionHash"];
    
    echo "<script>console.log('Auction Objects: $auction_id + $auction_contract_address + $transactionHash');</script>";		

    $updateDeliveryQuery = "UPDATE delivery SET status='completed' where auctionID = '$auction_id'";
    mysqli_query($connect, $updateDeliveryQuery);
    if (mysqli_affected_rows($connect) >0)
    {
        $updateEscrowQuery = "UPDATE escrow SET status='completed', transactionHash='$transactionHash' where auctionID = '$auction_id'";
        mysqli_query($connect, $updateEscrowQuery);
        if (mysqli_affected_rows($connect) >0)
        {
            $updateAuctionQuery = "UPDATE auction SET status='completed' where auctionID = '$auction_id'";
            mysqli_query($connect, $updateAuctionQuery);
            if (mysqli_affected_rows($connect) >0)
            {
                $disputeExistResult = mysqli_query($connect, "SELECT * from dispute WHERE AuctionID = '$auction_id'");
                $disputeExist = mysqli_fetch_assoc($disputeExistResult);
                if(mysqli_num_rows($disputeExistResult) > 0)
                {
                    $updateDisputeQuery = "UPDATE dispute SET status = 'closed' WHERE disputeID = '{$disputeExist['DisputeID']}'";
                    mysqli_query($connect, $updateDisputeQuery);
                    if (mysqli_affected_rows($connect) >0)
                    {
                        $reply = 5;
                        $status = "success"; 
                        $response = "Update delivery, escrow, auction and dispute successful. No dispute found";
                    }
                    else
                    {
                        $reply = 6;
                        $status = "failed"; 
                        $response = "Update dispute unsuccessful.";  
                        ?>
                        <script type="text/javascript">
                            console.log('Error: <?= mysqli_error($connect); ?>');
                        </script>
                        <?php
                    }
                }
                else
                {
                    $reply = 3;
                    $status = "success"; 
                    $response = "Update delivery, escrow and auction successful. No dispute found"; 
                } 
            }
            else
            {
                $reply = 4;
                $status = "failed"; 
                $response = "Update auction unsuccessful.";  
                ?>
                <script type="text/javascript">
                    console.log('Error: <?= mysqli_error($connect); ?>');
                </script>
                <?php
            }
        }
        else
        {
            $reply = 2;
            $status = "failed"; 
            $response = "Update escrow unsuccessful.";  
            ?>
            <script type="text/javascript">
                console.log('Error: <?= mysqli_error($connect); ?>');
            </script>
            <?php
        }
    }
    else
    {
        $reply = 1;
        $status = "failed"; 
        $response = "Update delivery unsuccessful."; 
        ?>
        <script type="text/javascript">
            console.log('Error: <?= mysqli_error($connect); ?>');
        </script>
        <?php  
    }
?>