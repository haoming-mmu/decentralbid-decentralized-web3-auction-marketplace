-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 10, 2024 at 09:37 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.3.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fyp_auction_marketplace`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `AdminID` varchar(11) NOT NULL,
  `Username` varchar(50) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auction`
--

CREATE TABLE `auction` (
  `AuctionID` varchar(11) NOT NULL,
  `auction_txhash` varchar(100) DEFAULT NULL,
  `auction_contract_address` varchar(100) DEFAULT NULL,
  `ItemName` varchar(100) DEFAULT NULL,
  `MinBid` decimal(10,2) DEFAULT NULL,
  `StartTime` datetime DEFAULT NULL,
  `EndTime` datetime DEFAULT NULL,
  `product_condition` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `duration` int(10) DEFAULT NULL,
  `reserved_price` decimal(10,2) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `product_front_image` varchar(200) DEFAULT NULL,
  `product_back_image` varchar(200) DEFAULT NULL,
  `category_id` varchar(7) DEFAULT NULL,
  `highest_bid_bidid` varchar(11) DEFAULT NULL,
  `winner_userid` varchar(11) DEFAULT NULL,
  `SellerUserID` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auction`
--

INSERT INTO `auction` (`AuctionID`, `auction_txhash`, `auction_contract_address`, `ItemName`, `MinBid`, `StartTime`, `EndTime`, `product_condition`, `description`, `duration`, `reserved_price`, `status`, `product_front_image`, `product_back_image`, `category_id`, `highest_bid_bidid`, `winner_userid`, `SellerUserID`) VALUES
('A0001', NULL, '0xc9cCD82476a5BBd52a89A64fe81EF506e3ddd30D', 'auction 1', NULL, '2023-12-25 17:23:00', '2023-12-25 17:26:00', 'brand new', 'red xl, teehee', 3, '0.10', 'completed', 'gamingpc.jpg', 'league PatFeniss.PNG', 'ELC', NULL, NULL, 'U0001'),
('A0002', NULL, '0xc17161FecCA1927B247E27917f0c3Fb2dDAfaD2E', 'auction 2', NULL, '2023-12-27 21:30:00', '2023-12-27 21:33:00', 'brand new', 'red, xl', 3, '0.10', 'completed', 'nike_airfore1_white.png', 'steam yunghm.PNG', 'ELC', NULL, NULL, 'U0001'),
('A0003', NULL, '0xCdB17AB4bEB2652caF0554CD501e34c64168A8BB', 'nike', NULL, '2023-12-28 00:24:00', '2023-12-28 00:27:00', 'brand new', 'red, size 8', 3, '0.10', 'active', 'bear.jpg', 'steam yunghm.PNG', 'ANC', NULL, NULL, 'U0001'),
('A0004', NULL, '0x5500Fb3C1899C1a7c69dCDE390379E8243c86c6e', 'sony', NULL, '2023-12-28 00:28:00', '2023-12-28 00:32:00', 'brand new', 'purple red', 4, '0.10', 'completed', 'adidas_sp_black.jpg', 'steam mr deagle.PNG', 'ELC', NULL, NULL, 'U0001'),
('A0005', NULL, '0x2553036A4a6Bc7b73BF234A25589dcEAb2B192EC', 'adidas', NULL, '2023-12-28 00:50:00', '2023-12-28 01:00:00', 'brand new', 'green x2', 10, '0.10', 'completed', 'nike_airfore1_brown.png', 'steam yunghm.PNG', 'OTH', NULL, NULL, 'U0001'),
('A0006', NULL, '0x93917C7bdDd160f76E1cef1Ab6Ac14e22b794152', 'toyz', NULL, '2023-12-28 00:51:00', '2023-12-28 01:01:00', 'used', 'red nerf gun', 10, '0.10', 'active', 'rolex_green.jpg', 'league PatFeniss.PNG', 'ELC', NULL, NULL, 'U0001'),
('A0007', NULL, '0x5BFC6F3B38a4081E873F4A87F34d60a139FeD063', 'yoyo', NULL, '2023-12-28 00:55:00', '2023-12-28 00:58:00', 'brand new', 'flower x5', 3, '0.13', 'active', 'steam mr deagle.PNG', 'league PatFeniss.PNG', 'OTH', NULL, NULL, 'U0001'),
('A0008', NULL, '0x98481A79a8ac65091f6345575C20FE67BAB75054', 'auction test no bid', NULL, '2024-01-01 13:05:00', '2024-01-01 13:07:00', 'brand new', 'no bid', 2, '0.10', 'active', 'league PatFeniss.PNG', 'steam mr deagle.PNG', 'ELC', NULL, NULL, 'U0001'),
('A0009', NULL, '0x23e628F39d35ae04E823dC5C51A172aB525F3771', 'auction test got bid', NULL, '2024-01-01 13:06:00', '2024-01-01 13:08:00', 'brand new', 'purple red', 2, '0.10', 'settlement', 'rolex_green.jpg', 'rolex_green.jpg', 'LUX', NULL, NULL, 'U0001'),
('A0010', NULL, '0x84528CFDB6B9Ed0A93950D7932F21540034285bA', 'test no bid 2', NULL, '2024-01-01 13:11:00', '2024-01-01 13:12:00', 'brand new', 'flower x5', 1, '0.10', 'active', 'steam yunghm.PNG', 'steam mr deagle.PNG', 'ELC', NULL, NULL, 'U0001'),
('A0011', NULL, '0x5eBCF4f3C85533718189ac2C99a997f5d61ecA81', 'test no bid 3', NULL, '2024-01-01 13:15:00', '2024-01-01 13:16:00', 'brand new', 'flower x2', 1, '0.10', 'completed', 'league PatFeniss.PNG', 'steam yunghm.PNG', 'ANC', NULL, NULL, 'U0001'),
('A0012', NULL, '0x3574390a89B63319A997F1A706d78E34A81EB7bb', 'test no bid 4', NULL, '2024-01-01 13:32:00', '2024-01-01 13:33:00', 'brand new', 'flower x5', 1, '0.10', 'completed', 'steam yunghm.PNG', 'steam mr deagle.PNG', 'ELC', NULL, NULL, 'U0001'),
('A0013', NULL, '0x4C5a902d98a1E63c56BdeC360364C46216be5C49', 'Lee Chong Wei Badminton Racket', NULL, '2024-01-05 19:16:00', '2024-01-05 19:26:00', 'brand new', 'Yonex, Blue', 10, '0.05', 'completed', 'leechongweirq_blue.jpg', 'lcwblueback.jpg', 'OTH', 'B0002', 'U0002', 'U0001'),
('A0014', NULL, '0x397F3b242FC0df5A47Eb811a34EF21847f4b7a91', 'Bearbrick Mickey Mouse', NULL, '2024-01-05 22:23:00', '2024-01-05 22:27:00', 'used', 'red, large size', 4, '0.10', 'completed', 'bearbrick_mickey.jpg', 'bearbrick_mickey2.jpg', 'ANC', 'B0003', 'U0004', 'U0001'),
('A0015', NULL, '0x1dFE5c17cD92Bf1d353E86C3E20603b1EF226D30', 'Patek Philippe Nautilus Black', NULL, '2024-01-05 22:24:00', '2024-01-05 22:28:00', 'used', 'Black', 4, '0.20', 'completed', 'patek_nautilus.jpg', 'patek_black.jpg', 'LUX', 'B0004', 'U0004', 'U0001'),
('A0016', NULL, '0x37dB6a76035B6EF497B9C6915c8c151228afA80A', 'Patek Philippe Nautilus Gold', NULL, '2024-01-07 14:11:00', '2024-01-07 14:21:00', 'brand new', 'Gold Black', 10, '0.50', 'active', 'patek_gold.jpg', 'patek_gold.jpg', 'LUX', NULL, NULL, 'U0001'),
('A0017', NULL, '0x454f3d1ceC6875d11e80b9c09bF18D3C0b11B972', 'Patek Philippe Nautilus Brown', NULL, '2024-01-07 14:33:00', '2024-01-07 14:43:00', 'used', 'Brown, 2019', 10, '0.50', 'settlement', 'patek_brown.jpg', 'patek_gold.jpg', 'LUX', 'B0005', 'U0004', 'U0001'),
('A0018', NULL, '0xa87a98d14F3D8a61E6C0ACcc4D9FC80B28b56a57', 'pamp gold bar 50G', NULL, '2024-01-08 21:22:00', '2024-01-08 21:37:00', 'brand new', '50G, With certificate', 15, '2.00', 'completed', 'pamp 50g front.jpg', '', 'ANC', 'B0008', 'U0002', 'U0001'),
('A0019', NULL, '0x823c391d6720087Ebe92731d4FA7D2a5ea7052e4', 'Chanel black heels 2023', NULL, '2024-01-08 21:22:00', '2024-01-08 21:37:00', 'used', 'Size UK 5', 15, '0.50', 'completed', 'chanel heels black front.jpg', '', 'LUX', 'B0009', 'U0002', 'U0001'),
('A0020', NULL, '0x4E83E4824A29a041971Bc07a385fBB0a475FB934', 'Gucci T shirt black ', NULL, '2024-01-08 21:34:00', '2024-01-08 21:37:00', 'brand new', 'Size L', 3, '0.25', 'active', 'gucci black tshirt.jpg', 'gucci black tshirt_back.jpg', 'LUX', NULL, NULL, 'U0001'),
('A0021', NULL, '0x7ee2800C1aCb13A1e4a293AC9533c81281089f03', 'Badminton Racket LCW', NULL, '2024-01-08 21:40:00', '2024-01-08 21:43:00', 'used', 'Blue', 3, '0.30', 'completed', 'leechongweirq_blue.jpg', 'lcwblueback.jpg', 'OTH', 'B0013', 'U0002', 'U0001'),
('A0022', NULL, '0xE9933c445D48602DFa3F476c92696B7Acd40d83b', 'auction test release', NULL, '2024-01-09 21:48:00', '2024-01-09 21:49:00', 'brand new', 'purple red', 1, '0.10', 'active', 'bearbrick_mickey.jpg', 'bearbrick_mickey2.jpg', 'ELC', NULL, NULL, 'U0002'),
('A0023', NULL, '0x40b16E39DEF893F3f7f4F42735A3Fac4F5516E5C', 'test bearbrick', NULL, '2024-01-08 21:50:00', '2024-01-08 21:52:00', 'brand new', 'purple red', 2, '0.13', 'completed', 'bearbrick_mickey.jpg', 'bearbrick_mickey2.jpg', 'ANC', 'B0015', 'U0002', 'U0001'),
('A0024', NULL, '0xDc35570EC9F4c1a737F1846b876eEE03fF25feC3', 'bearbrick test3', NULL, '2024-01-08 21:57:00', '2024-01-08 21:59:00', 'brand new', 'green ', 2, '0.12', 'completed', 'bearbrick_mickey.jpg', 'bearbrick_mickey2.jpg', 'ANC', 'B0017', 'U0002', 'U0001'),
('A0025', NULL, '0x9424020dA380ea1afb0f75a072255faEF46336FE', 'LCW racquet', NULL, '2024-01-08 22:04:00', '2024-01-08 22:07:00', 'used', 'blue', 3, '0.20', 'active', 'leechongweirq_blue.jpg', 'lcwblueback.jpg', 'OTH', NULL, NULL, 'U0001'),
('A0026', NULL, '0xDC3f87EE032aC789ef15E7A7AdAE4C187ee2Bd64', 'LCW blue racquet Presentation', NULL, '2024-01-08 22:10:00', '2024-01-08 22:13:00', 'used', 'Blue ', 3, '0.20', 'completed', 'leechongweirq_blue.jpg', 'lcwblueback.jpg', 'OTH', 'B0020', 'U0002', 'U0001'),
('A0027', NULL, '0xd6D39520d173f455D1f91580e420aB432Cb08c61', 'Pamp Gold Bar 50G', NULL, '2024-01-08 22:23:00', '2024-01-08 22:25:00', 'brand new', '50G, With certificate', 2, '2.00', 'completed', 'pamp 50g front.jpg', 'pamp 50g back.jpg', 'ANC', 'B0021', 'U0002', 'U0001'),
('A0028', NULL, '0x684c8DAdE586cbCb3E61317A5601E3F46Ad2641A', 'Chanel black heels 2023', NULL, '2024-01-08 22:24:00', '2024-01-08 22:27:00', 'used', 'Size UK 5', 3, '0.50', 'completed', 'chanel heels black front.jpg', 'chanel heels black back.jpg', 'LUX', 'B0022', 'U0002', 'U0001'),
('A0029', NULL, '0xa5385cbC8D0a9A737Eb8B14B1cd82C18bFF765d0', 'test auction 1', NULL, '2024-01-10 14:36:00', '2024-01-10 14:42:00', 'brand new', 'red xl, teehee', 6, '0.20', 'completed', 'pamp 50g front.jpg', 'pamp 50g back.jpg', 'ELC', 'B0024', 'U0004', 'U0001'),
('A0030', NULL, '0x0B3299dC946c983106D4f34aACaeba55092338c2', 'test auction 3', NULL, '2024-01-10 15:47:00', '2024-01-10 15:50:00', 'brand new', 'red xl, teehee', 3, '0.10', 'active', 'pamp 50g front.jpg', 'pamp 50g back.jpg', 'LUX', NULL, NULL, 'U0001'),
('A0031', NULL, '0xF919e364361BAFa77404910B1d9385c1E00F46F6', 'test auction 4', NULL, '2024-01-10 15:52:00', '2024-01-10 15:57:00', 'brand new', 'purple red', 5, '0.10', 'completed', 'pamp 50g front.jpg', 'pamp 50g back.jpg', 'ELC', 'B0028', 'U0005', 'U0001'),
('A0032', NULL, '0xBD456152789f5C8773CBeC88b9718302fB9c0eBF', 'test auction 5', NULL, '2024-01-10 16:03:00', '2024-01-10 16:07:00', 'brand new', 'purple red', 4, '0.10', 'active', 'pamp 50g front.jpg', 'pamp 50g back.jpg', 'LUX', NULL, NULL, 'U0001');

-- --------------------------------------------------------

--
-- Table structure for table `bid`
--

CREATE TABLE `bid` (
  `BidID` varchar(11) NOT NULL,
  `Amount` decimal(10,2) DEFAULT NULL,
  `TimePlaced` datetime DEFAULT NULL,
  `transactionHash` varchar(100) NOT NULL,
  `auction_contract_address` varchar(100) NOT NULL,
  `AuctionID` varchar(11) DEFAULT NULL,
  `BuyerUserID` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bid`
--

INSERT INTO `bid` (`BidID`, `Amount`, `TimePlaced`, `transactionHash`, `auction_contract_address`, `AuctionID`, `BuyerUserID`) VALUES
('B0001', '0.10', '2024-01-05 19:18:31', '0x6e3e0e840f864630581f9dae5029ec23edc6151cb83a1953a19497fcbda074d9', '0x4C5a902d98a1E63c56BdeC360364C46216be5C49', 'A0013', 'U0004'),
('B0002', '0.12', '2024-01-05 19:19:36', '0xd6c37bb02635ac8f3dc7e1383fac7349e972ff9ee4d13cc9a2405007f76dcf18', '0x4C5a902d98a1E63c56BdeC360364C46216be5C49', 'A0013', 'U0002'),
('B0003', '0.15', '2024-01-05 22:25:12', '0x58b92b79692336177ce19bc39cd74bcf17fa7f3e0e29f9e855b2a5b98eadc775', '0x397F3b242FC0df5A47Eb811a34EF21847f4b7a91', 'A0014', 'U0004'),
('B0004', '0.35', '2024-01-05 22:25:58', '0x781c18e267d24f8ce0be03fe97d2ff302caea4f52bb92d8dd1c1fa23d23827df', '0x1dFE5c17cD92Bf1d353E86C3E20603b1EF226D30', 'A0015', 'U0004'),
('B0005', '0.66', '2024-01-07 14:36:42', '0x43a4384fe8a11bb2c04e4c1630fa34f86ad166e521821ed1d54c8dcbda16b9d5', '0x454f3d1ceC6875d11e80b9c09bF18D3C0b11B972', 'A0017', 'U0004'),
('B0006', '0.62', '2024-01-07 14:42:14', '0x3841bc5d3dbb552b851222f92ac3260bc6661a3d13b171a2a0970f47699d0800', '0x454f3d1ceC6875d11e80b9c09bF18D3C0b11B972', 'A0017', 'U0002'),
('B0007', '2.20', '2024-01-08 21:24:03', '0xe77561380200d4b674d5f06fea740802cd7f1e0f955becc035622dad90bd114d', '0xa87a98d14F3D8a61E6C0ACcc4D9FC80B28b56a57', 'A0018', 'U0004'),
('B0008', '3.00', '2024-01-08 21:24:38', '0xc2f82069a5ef17a46a11e572cd01c69d684e78fac754772e0136f9bcbc9814b3', '0xa87a98d14F3D8a61E6C0ACcc4D9FC80B28b56a57', 'A0018', 'U0002'),
('B0009', '1.00', '2024-01-08 21:25:21', '0x0cc27814663d9fa8910b4d98194bb06b96b9dda3edfebc2f214d867956e0cf69', '0x823c391d6720087Ebe92731d4FA7D2a5ea7052e4', 'A0019', 'U0002'),
('B0010', '0.70', '2024-01-08 21:25:55', '0x56adad7e70c1f8fd197b094e6aab8ae3d0f16b382d476940eac7c0b74f1a88ad', '0x823c391d6720087Ebe92731d4FA7D2a5ea7052e4', 'A0019', 'U0004'),
('B0011', '0.40', '2024-01-08 21:35:34', '0x0aafd45823a8147bdd39e65d9c78d9316c7dc83727c0e888c3005e51ff8acdef', '0x4E83E4824A29a041971Bc07a385fBB0a475FB934', 'A0020', 'U0004'),
('B0012', '0.35', '2024-01-08 21:40:45', '0x6dffb4af1f404e87c522362627b68c110de6ec8b0411973be67466793d73bc8d', '0x7ee2800C1aCb13A1e4a293AC9533c81281089f03', 'A0021', 'U0004'),
('B0013', '0.50', '2024-01-08 21:41:38', '0xdee350bc9d345a8a51db4b17233a8d8840e4c23be4e8898b13a9bad8f42bd229', '0x7ee2800C1aCb13A1e4a293AC9533c81281089f03', 'A0021', 'U0002'),
('B0014', '0.15', '2024-01-08 21:50:22', '0x46edd6e972239ee3a80bb1c70c57233772c201aca61a32c13a791ded89cbe823', '0x40b16E39DEF893F3f7f4F42735A3Fac4F5516E5C', 'A0023', 'U0004'),
('B0015', '0.20', '2024-01-08 21:50:49', '0x54b6f647106b9e0bdfd75bf3bbd896c56e4e129aa4d376b6496a3463d21fe918', '0x40b16E39DEF893F3f7f4F42735A3Fac4F5516E5C', 'A0023', 'U0002'),
('B0016', '0.15', '2024-01-08 21:58:07', '0x011b76a60e0a370e10c626a9f9f6e9d57cda174129ab492cefffca3d3bf0dfae', '0xDc35570EC9F4c1a737F1846b876eEE03fF25feC3', 'A0024', 'U0004'),
('B0017', '0.20', '2024-01-08 21:58:34', '0x5202bcf09ec7ecde1d6acd7395a3dc8fd2e6c195f7756995a12d1f2dfcbaccaa', '0xDc35570EC9F4c1a737F1846b876eEE03fF25feC3', 'A0024', 'U0002'),
('B0018', '0.25', '2024-01-08 22:05:08', '0xa3f44ab8f686c5caa9ef393b9a4dbce3a14f4cc6f5bc70cc4156433b908c5e76', '0x9424020dA380ea1afb0f75a072255faEF46336FE', 'A0025', 'U0004'),
('B0019', '0.25', '2024-01-08 22:11:24', '0x05a61aa3a90e53e5a982f24cd93ed81db5ecad0a82385aab262f368b4eaa3e3b', '0xDC3f87EE032aC789ef15E7A7AdAE4C187ee2Bd64', 'A0026', 'U0004'),
('B0020', '0.30', '2024-01-08 22:12:34', '0x9266468379bf2b0f01677110be8e85ba9ade23691f87334be51f105826825ff3', '0xDC3f87EE032aC789ef15E7A7AdAE4C187ee2Bd64', 'A0026', 'U0002'),
('B0021', '3.00', '2024-01-08 22:24:39', '0x188df44c92cd6256dd964690f355acae0dfda7278a082540f492c0068dd07a4d', '0xd6D39520d173f455D1f91580e420aB432Cb08c61', 'A0027', 'U0002'),
('B0022', '0.80', '2024-01-08 22:25:11', '0x61fed372306c9dec930cddd235d66e05ded55dc4ff95d4cecfbdf12962295527', '0x684c8DAdE586cbCb3E61317A5601E3F46Ad2641A', 'A0028', 'U0002'),
('B0023', '0.30', '2024-01-10 14:37:23', '0xc8a1bf5a5fb049180cf740546a14cceb5d42f3538a03fe4d07b49e7921537d11', '0xa5385cbC8D0a9A737Eb8B14B1cd82C18bFF765d0', 'A0029', 'U0004'),
('B0024', '0.40', '2024-01-10 14:38:01', '0xaf8b2b238aaa91b539bcd62a3ebdba55d7ab9e270471104d7f7e0c605b61d213', '0xa5385cbC8D0a9A737Eb8B14B1cd82C18bFF765d0', 'A0029', 'U0004'),
('B0025', '0.35', '2024-01-10 14:38:34', '0x7e0685b36df879a2f635e4651757c729d62abe34094450fb189df3743b6778ae', '0xa5385cbC8D0a9A737Eb8B14B1cd82C18bFF765d0', 'A0029', 'U0002'),
('B0026', '0.11', '2024-01-10 15:54:17', '0xa4dce86b79ea8c56314bc4dd26fd02688515842f9546b4950706dd5ab3fa773e', '0xF919e364361BAFa77404910B1d9385c1E00F46F6', 'A0031', 'U0004'),
('B0027', '0.21', '2024-01-10 15:54:47', '0x6867916f95f2366e51b72f5f1686edcd600785cea8b70e6d2df96e11937c22a8', '0xF919e364361BAFa77404910B1d9385c1E00F46F6', 'A0031', 'U0002'),
('B0028', '0.31', '2024-01-10 15:55:22', '0xa29f322959e45dd9ddf5074032edfc8f0a839a6c144cbd28680397f41a0793e1', '0xF919e364361BAFa77404910B1d9385c1E00F46F6', 'A0031', 'U0005'),
('B0029', '0.20', '2024-01-10 16:04:00', '0xe13f572ba0dcf828126dcfc53108ad24c2b504d203a289067c31fd5bffb3c01c', '0xBD456152789f5C8773CBeC88b9718302fB9c0eBF', 'A0032', 'U0004');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` varchar(7) NOT NULL,
  `category_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`) VALUES
('ANC', 'Art & Collectibles'),
('ELC', 'Electronics'),
('LUX', 'Luxury'),
('OTH', 'Others');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `UserID` varchar(11) NOT NULL,
  `Username` varchar(50) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `WalletAddress` varchar(50) DEFAULT NULL,
  `first_name` varchar(128) NOT NULL,
  `last_name` varchar(128) NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `postcode` varchar(11) NOT NULL,
  `state` varchar(50) NOT NULL,
  `phone_number` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`UserID`, `Username`, `Email`, `WalletAddress`, `first_name`, `last_name`, `address1`, `address2`, `city`, `postcode`, `state`, `phone_number`) VALUES
('U0001', NULL, '', '0xfcfc92420adfa84fdfae6140367f44b22c46a5e1', '', '', '', '', '', '', '', ''),
('U0002', NULL, NULL, '0x4794b7c69523e273875dd99fcba10c82b157a1b2', '', '', '', '', '', '0', '', '0'),
('U0003', NULL, NULL, '0xc40790e817b74d1be1df8552e3020b778b6c9021', '', '', '', '', '', '0', '', '0'),
('U0004', NULL, '1231r1asd@gmail.com', '0x771aba91293d3d5a98c5d1f86520d350dc71bd6b', '124r3rfwqwqwege', '123123123', 'adasd123', 'Jalan Rehab', 'KL', '53623', 'KL', '12312123'),
('U0005', NULL, NULL, '0x3845c6b12abee7492ea2ba9a0eefa524affc2775', '', '', '', '', '', '0', '', '0'),
('U0006', NULL, NULL, '0x7ca12668101b2668d50ea350b1741c29fce6d6d1', '', '', '', '', '', '0', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `delivery`
--

CREATE TABLE `delivery` (
  `deliveryID` varchar(11) NOT NULL,
  `address` text DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `auctionID` varchar(11) DEFAULT NULL,
  `SellerUserID` varchar(11) DEFAULT NULL,
  `BuyerUserID` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `delivery`
--

INSERT INTO `delivery` (`deliveryID`, `address`, `status`, `auctionID`, `SellerUserID`, `BuyerUserID`) VALUES
('D0001', 'APU, TPM, 50356 KL', 'completed', 'A0013', 'U0001', 'U0002'),
('D0002', '23, Jalan Jalil 1, 53768 KL', 'receiving', 'A0014', 'U0001', 'U0004'),
('D0003', '23, Jalan Jalil 1, 53768 KL', 'receiving', 'A0015', 'U0001', 'U0004'),
('D0004', 'Apu Malaysia, 52631 KL', 'completed', 'A0021', 'U0001', 'U0002'),
('D0005', 'apu malaysia', 'completed', 'A0023', 'U0001', 'U0002'),
('D0006', 'apu malaysia', 'completed', 'A0024', 'U0001', 'U0002'),
('D0007', 'APU Malaysia', 'completed', 'A0026', 'U0001', 'U0002'),
('D0008', 'Apu Malaysia', 'receiving', 'A0018', 'U0001', 'U0002'),
('D0009', 'Apu Malaysia', 'receiving', 'A0019', 'U0001', 'U0002'),
('D0010', 'Apu Malaysia', 'receiving', 'A0027', 'U0001', 'U0002'),
('D0011', 'APU Malaysia', 'receiving', 'A0028', 'U0001', 'U0002'),
('D0012', 'APU KL', 'completed', 'A0029', 'U0001', 'U0004'),
('D0013', 'APU MY', 'completed', 'A0031', 'U0001', 'U0005');

-- --------------------------------------------------------

--
-- Table structure for table `dispute`
--

CREATE TABLE `dispute` (
  `DisputeID` varchar(11) NOT NULL,
  `Description` text DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `AuctionID` varchar(11) DEFAULT NULL,
  `RaisedByUserID` varchar(11) DEFAULT NULL,
  `EscrowID` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dispute`
--

INSERT INTO `dispute` (`DisputeID`, `Description`, `status`, `AuctionID`, `RaisedByUserID`, `EscrowID`) VALUES
('DS0001', 'Colour not same as described', 'refunded', 'A0014', 'U0004', 'E0002'),
('DS0002', 'Watch is fake', 'rejected', 'A0015', 'U0004', 'E0003'),
('DS0003', 'Gold is fake', 'refunded', 'A0018', 'U0002', 'E0008'),
('DS0004', 'Size is wrong', 'rejected', 'A0019', 'U0002', 'E0009'),
('DS0005', 'Gold is fake', 'refunded', 'A0027', 'U0002', 'E0010'),
('DS0006', 'Size is wrong', 'rejected', 'A0028', 'U0002', 'E0011');

-- --------------------------------------------------------

--
-- Table structure for table `escrow`
--

CREATE TABLE `escrow` (
  `escrowID` varchar(11) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `transactionHash` varchar(100) DEFAULT NULL,
  `deliveryID` varchar(11) DEFAULT NULL,
  `auctionID` varchar(11) DEFAULT NULL,
  `SellerUserID` varchar(11) DEFAULT NULL,
  `BuyerUserID` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `escrow`
--

INSERT INTO `escrow` (`escrowID`, `status`, `amount`, `transactionHash`, `deliveryID`, `auctionID`, `SellerUserID`, `BuyerUserID`) VALUES
('E0001', 'completed', '0.12', '0xa30ff670f149da32981b1821a1faf29574831747dcca97e739112516de60d160', 'D0001', 'A0013', 'U0001', 'U0002'),
('E0002', 'completed', '0.15', '0x02e5169eaa4253def91cb4cd9901bc8b8858570e63c9b7eff0e515a6c387f1ff', 'D0002', 'A0014', 'U0001', 'U0004'),
('E0003', 'completed', '0.35', '0x1c949451cccb53955c504b507d18e1a199f0bd2ccc5f4773aebe638cc40e7a8d', 'D0003', 'A0015', 'U0001', 'U0004'),
('E0004', 'completed', '0.50', '0x67bac89946ff0bc615cbe8449bba19a741cbdd898f7b4f3f6f37a7450ee828b5', 'D0004', 'A0021', 'U0001', 'U0002'),
('E0005', 'completed', '0.20', '0x891a128195900203a5bcc55d9382745ec35f6eabacfff78a3314f0dd1571238d', 'D0005', 'A0023', 'U0001', 'U0002'),
('E0006', 'completed', '0.20', '0x0e9bd2beaf63d4980fc28f7378526689fedc2961b267e26b258e660421671c5d', 'D0006', 'A0024', 'U0001', 'U0002'),
('E0007', 'completed', '0.30', '0x9a49e6558d7d21ec8848303ccdf0817fb370c264cce5e49a3766b8bd8118e4f5', 'D0007', 'A0026', 'U0001', 'U0002'),
('E0008', 'completed', '3.00', '0x515152d8179825a86b8ac9136650ea8849cda6355362c02e48f2c963e12b4c2e', 'D0008', 'A0018', 'U0001', 'U0002'),
('E0009', 'completed', '1.00', '0xa77108598b83da94ff40043e542b13b0327e7acf7ea5e62969f9780eb405351a', 'D0009', 'A0019', 'U0001', 'U0002'),
('E0010', 'completed', '3.00', '0xe30b192b50c77ec4a3a6b14fe5a068ffd489ad290d941d5dabbdff220900d2cd', 'D0010', 'A0027', 'U0001', 'U0002'),
('E0011', 'completed', '0.80', '0xb607b456cf62afc4f418a70f533c36fb75885d8c7018ebec94f2955249c40ef5', 'D0011', 'A0028', 'U0001', 'U0002'),
('E0012', 'completed', '0.40', '0xe2300e4f7e0a620b5b925ac6110ea79c299f3729d02dd11462f8ee83321bafc1', 'D0012', 'A0029', 'U0001', 'U0004'),
('E0013', 'completed', '0.31', '0xec7c4addd1e28f039c313aec3fd20c185337113aa8b2d9707d34567eacbae058', 'D0013', 'A0031', 'U0001', 'U0005');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`AdminID`);

--
-- Indexes for table `auction`
--
ALTER TABLE `auction`
  ADD PRIMARY KEY (`AuctionID`),
  ADD KEY `auction_ibfk_1` (`SellerUserID`),
  ADD KEY `fk_category` (`category_id`),
  ADD KEY `fk_bid` (`highest_bid_bidid`),
  ADD KEY `fk_customer` (`winner_userid`);

--
-- Indexes for table `bid`
--
ALTER TABLE `bid`
  ADD PRIMARY KEY (`BidID`),
  ADD KEY `bid_ibfk_2` (`BuyerUserID`),
  ADD KEY `bid_ibfk_1` (`AuctionID`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`UserID`);

--
-- Indexes for table `delivery`
--
ALTER TABLE `delivery`
  ADD PRIMARY KEY (`deliveryID`),
  ADD KEY `auctionID` (`auctionID`),
  ADD KEY `SellerUserID` (`SellerUserID`),
  ADD KEY `BuyerUserID` (`BuyerUserID`);

--
-- Indexes for table `dispute`
--
ALTER TABLE `dispute`
  ADD PRIMARY KEY (`DisputeID`),
  ADD KEY `AuctionID` (`AuctionID`),
  ADD KEY `EscrowID` (`EscrowID`);

--
-- Indexes for table `escrow`
--
ALTER TABLE `escrow`
  ADD PRIMARY KEY (`escrowID`),
  ADD KEY `deliveryID` (`deliveryID`),
  ADD KEY `auctionID` (`auctionID`),
  ADD KEY `SellerUserID` (`SellerUserID`),
  ADD KEY `BuyerUserID` (`BuyerUserID`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auction`
--
ALTER TABLE `auction`
  ADD CONSTRAINT `auction_ibfk_1` FOREIGN KEY (`SellerUserID`) REFERENCES `customer` (`UserID`),
  ADD CONSTRAINT `fk_bid` FOREIGN KEY (`highest_bid_bidid`) REFERENCES `bid` (`BidID`),
  ADD CONSTRAINT `fk_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`),
  ADD CONSTRAINT `fk_customer` FOREIGN KEY (`winner_userid`) REFERENCES `customer` (`UserID`);

--
-- Constraints for table `bid`
--
ALTER TABLE `bid`
  ADD CONSTRAINT `bid_ibfk_1` FOREIGN KEY (`AuctionID`) REFERENCES `auction` (`AuctionID`),
  ADD CONSTRAINT `bid_ibfk_2` FOREIGN KEY (`BuyerUserID`) REFERENCES `customer` (`UserID`);

--
-- Constraints for table `delivery`
--
ALTER TABLE `delivery`
  ADD CONSTRAINT `delivery_ibfk_1` FOREIGN KEY (`auctionID`) REFERENCES `auction` (`AuctionID`),
  ADD CONSTRAINT `delivery_ibfk_2` FOREIGN KEY (`SellerUserID`) REFERENCES `customer` (`UserID`),
  ADD CONSTRAINT `delivery_ibfk_3` FOREIGN KEY (`BuyerUserID`) REFERENCES `customer` (`UserID`);

--
-- Constraints for table `dispute`
--
ALTER TABLE `dispute`
  ADD CONSTRAINT `dispute_ibfk_1` FOREIGN KEY (`EscrowID`) REFERENCES `escrow` (`escrowID`);

--
-- Constraints for table `escrow`
--
ALTER TABLE `escrow`
  ADD CONSTRAINT `escrow_ibfk_1` FOREIGN KEY (`deliveryID`) REFERENCES `delivery` (`deliveryID`),
  ADD CONSTRAINT `escrow_ibfk_2` FOREIGN KEY (`auctionID`) REFERENCES `auction` (`AuctionID`),
  ADD CONSTRAINT `escrow_ibfk_3` FOREIGN KEY (`SellerUserID`) REFERENCES `customer` (`UserID`),
  ADD CONSTRAINT `escrow_ibfk_4` FOREIGN KEY (`BuyerUserID`) REFERENCES `customer` (`UserID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
